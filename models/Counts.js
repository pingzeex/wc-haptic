// var keystone = require('keystone');
// var Types = keystone.Field.Types;

// var Count = new keystone.List('Count',
// {
//     track: true,
//     autokey: { from: 'user', path: 'key', unique: true },

// });

// Count.add({
//     user: { type: Types.Relationship, ref: 'User', index: true },
//     offline: { type: Types.Number, default: 0, index: true },
//     online: { type: Types.Number, default: 0, index: true },
//     faulty: { type: Types.Number, default: 0, index: true },
//     none: { type: Types.Number, default: 0, index: true },
//     time: { type: Types.Datetime, required: true, default:Date.now ,index:true },

// });

// Count.defaultColumns = ' online, offline, faulty, none ,time' ;
// Count.register();

var keystone = require('keystone');
// var mongoosePaginate = require('mongoose-paginate');s
var Types = keystone.Field.Types;

/**
 * Count Model
 * ==========
 */

var CountNode = new keystone.List('CountNode', {
	// map: { name: 'title' },
	autokey: {
		path: 'slug',
		from: 'id',
		unique: true,
	},
});

CountNode.add({


	userKey: {
		type: Types.Relationship,
		ref: 'User',
		index: true,
	},
	offline: {
		type: Number,
		default: 0,
		inital: true,
		index: true,
	},
	online: {
		type: Number,
		default: 0,
		inital: true,
		index: true,
	},
	faulty: {
		type: Number,
		default: 0,
		index: true,
	},
	none: {
		type: Number,
		default: 0,
		index: true,
	},
	time: {
		type: Types.Datetime,
		required: true,
		default: Date.now,
		index: true,
	},


});


// Count.schema.virtual('content.full').get(function () {
//     return this.content.extended || this.content.brief;
// });
CountNode.defaultColumns = ' online, offline, faulty, none ,time';
// Count.register();

CountNode.register();
