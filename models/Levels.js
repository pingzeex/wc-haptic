// var keystone = require('keystone');
// var Types = keystone.Field.Types;

// var Count = new keystone.List('Count',
// {
//     track: true,
//     autokey: { from: 'user', path: 'key', unique: true },

// });

// Count.add({
//     user: { type: Types.Relationship, ref: 'User', index: true },
//     offline: { type: Types.Number, default: 0, index: true },
//     online: { type: Types.Number, default: 0, index: true },
//     faulty: { type: Types.Number, default: 0, index: true },
//     none: { type: Types.Number, default: 0, index: true },
//     time: { type: Types.Datetime, required: true, default:Date.now ,index:true },

// });


// Count.defaultColumns = ' online, offline, faulty, none ,time' ;
// Count.register();

var keystone = require('keystone');
// var mongoosePaginate = require('mongoose-paginate');s
var Types = keystone.Field.Types;

/**
 * Count Model
 * ==========
 */

var Levels = new keystone.List('Levels', {
	// map: { name: 'title' },
	autokey: {
		path: 'slug',
		from: 'id',
		unique: true,
	},
});

Levels.add({


	userKey: {
		type: Types.Relationship,
		ref: 'User',
		index: true,
	},
	binKey: {
		type: Types.Relationship,
		ref: 'Bin',
		index: true,
	},
	oLevel: {
		type: Types.Number,
		default: 0,
	},
	oLevelBin: {
		type: Types.Number,
		default: 0,
	},
	smokeLevel: {
		type: Types.Number,
		default: 0,
	},
	smokeLevelBin: {
		type: Types.Number,
		default: 0,
	},
	aqLevel: {
		type: Types.Number,
		default: 0,
	},
	aqLevelBin: {
		type: Types.Number,
		default: 0,
	},
	inoLevel: {
		type: Types.Number,
		default: 0,
	},
	inoLevelBin: {
		type: Types.Number,
		default: 0,
	},
	time: {
		type: Types.Datetime,
		required: true,
		default: Date.now,
		index: true,
	},


});


// Count.schema.virtual('content.full').get(function () {
//     return this.content.extended || this.content.brief;
// });
Levels.defaultColumns = ' oLevel, smokeLevel, aqLevel, inoLevel ,time';
// Count.register();

Levels.register();
