var keystone = require('keystone');
require('dotenv').config();
var Types = keystone.Field.Types;
var mongoosePaginate = require('mongoose-paginate');
const mongooseTrack = require('mongoose-track');
var shortid = require('shortid');
var driver = {};
if (process.env.DEPLOY_MODE == 'production' || 'testing') {
	driver = require('socket.io-client')('http://localhost:6001/ops');

	driver.on('connect', function () {

		console.log('[Bin model connected] - ' + process.env.DEPLOY_MODE);


	});

}


// function getName () {

// 	let name = uniqid('pingzee-') + uniqid.time();

// 	return name;

// }
/**
 * Bin Model
 * ==========
 */

var Bin = new keystone.List('Bin', {
	map: {
		name: 'title',
	},
	autokey: {
		path: 'slug',
		from: 'title',
		unique: true,
	},
	track: true,
});
Bin.add({
	title: {
		type: String,
		required: true,
		index: true,
	},
	state: {
		type: Types.Select,
		options: 'maintenance,active,fault',
		default: 'active',
		index: true,
	},
	binKey: {
		type: Types.Key,
		// required: true,
		unique: true,
		default: shortid.generate,
		index: true,
	},
	createdDate: {
		type: Types.Datetime,
		default: Date.now,
		index: true,
	},
	oLevel: {
		type: Types.Number,
		default: 0,
	},
	smokeLevel: {
		type: Types.Number,
		default: 0,
	},
	aqLevel: {
		type: Types.Number,
		default: 0,
	},
	inoLevel: {
		type: Types.Number,
		default: 0,
	},
	notice: {
		type: Types.Number,
		default: 0,
	},
	status: {
		type: Types.Select,
		options: 'none,offline,online,faulty,',
		default: 'offline',
		index: true,
	},
	user: {
		type: Types.Relationship,
		ref: 'User',
		index: true,
	},
});

Bin.track = {
	createdAt: true,
	createdBy: true,
};


Bin.schema.pre('save', function (next) {
	this.wasNew = !this.isNew;
	console.log('checking', this.wasNew);
	next();
});

Bin.schema.post('remove', function (next) {
	// this.wasNew = !this.isNew;
	if (process.env.DEPLOY_MODE === 'production' || 'testing') {
		// console.log("");

		driver.emit('deviceRemove', this);

	}
	console.log('checking', this.wasNew);
});

Bin.schema.post('save', function (next) {

	if (this.wasNew) {
		if (process.env.DEPLOY_MODE === 'production' || 'testing') {

			// console.log(this);

			// driver.emit("deviceRemove", this);
			driver.emit('deviceUpdate', this);

		}
	}
});
Bin.schema.plugin(mongoosePaginate);
Bin.schema.plugin(mongooseTrack.plugin);
Bin.defaultColumns = 'title, state|20%, status|20%,, user|20%, createdDate|20%';
Bin.register();
