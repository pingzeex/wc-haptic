/* eslint-disable eqeqeq */
var socketio = require('socket.io');
var moment = require('moment');
var async = require('async');
var node_lineup = {};
var mongoose = require('mongoose');
var _ = require('underscore');
var key = 'mvzalkpwg';

var io;
var ops_io;
// var pipe_io;
var page_io;
var user_io;
var internal;

var keystone = require('keystone');
var Device = keystone.list('Bin');
var CountNode = keystone.list('CountNode');
var Level = keystone.list('Levels');


var nodeCounts = {


	time: Date.now(),
	offline: 0,
	online: 0,
	faulty: 0,

};
var binLevels = {


	time: Date.now(),
	oLevel: 0,
	inoLevel: 0,

};

/*  Command Decode function */

// function cmdDecode(cmd, userkey) {

// 	var query = {};
// 	if (cmd.includes("//")) {

// 		query.success = true;

// 		var initStage = cmd.split('//');

// 		query.user = userkey;

// 		var actionStage = initStage[1].split("-");

// 		/*  Commands verifications */

// 		switch (actionStage[0]) {
// 			case "tb":
// 				query.cmd = "tb";
// 				if (actionStage.length < 4) {

// 					query.success = false
// 					query.comment = "TalkBack command failed -> Incomplete command "
// 					return query;

// 				}

// 				query.content = {
// 					device: actionStage[1],
// 					channel: actionStage[2],
// 					data: actionStage[3]
// 				};


// 				break;
// 			case "link":
// 				query.cmd = "link";
// 				if (actionStage.length < 3) {

// 					query.success = false
// 					query.comment = "Link command failed -> Incomplete command "
// 					return query;

// 				}

// 				query.content = {
// 					pipe: actionStage[1],
// 					device: actionStage[2]
// 					// devices: []
// 				};

// 				/*  Multi Device support will be added later - in works */

// 				/* 	_.each(actionStage, function (val, key) {

// 					if (key > 1) {

// 						query.content.devices.push(val);
// 					}

// 				});
//  */


// 				break;
// 			case "unlink":

// 				query.cmd = "unlink";
// 				if (actionStage.length < 3) {

// 					query.success = false
// 					query.comment = "Unlink command failed -> Incomplete command "
// 					return query;

// 				}

// 				query.content = {
// 					pipe: actionStage[1],
// 					devices: []
// 				};

// 				_.each(actionStage, function (val, key) {

// 					if (key > 1) {

// 						query.content.devices.push(val);
// 					}

// 				});

// 				break;
// 			case "add":

// 				break;
// 			case "remove":

// 				break;
// 			case "sandbox":
// 				query.cmd = "sandbox";
// 				if (actionStage.length < 3) {

// 					query.success = false
// 					query.comment = "Sandbox command failed -> Incomplete command"
// 					return query;

// 				}

// 				query.content = {
// 					action: actionStage[1],
// 					devices: []
// 				};
// 				_.each(actionStage, function (val, key) {

// 					if (key > 1) {

// 						query.content.devices.push(val);
// 					}

// 				});

// 				break;

// 			default:
// 				break;
// 		}

// 	} else {
// 		query.success = false;
// 		query.comment = "Nice try but its not a command ";
// 	}


// 	return query;


// }


/*  Function to Update Bin  */

function binUpdate (key, change) {

	Device.model.findOne({ binKey: key }, function (err, device) {

		if (err) {

			console.log('bin update--while checking the device ---', err);


		}

		else {

			if (device) {


				device.oLevel = change.oLevel;


				device.save(function (err) {

					if (err) {

						console.log('update--While updating device --', err);

					}

				});

			}


		}


	});


}


/*
 Function Level
 */

var doLevel = function (socket, data, change) {


	var notice = false;


	async.series([

		// Perform basic validation
		function (next) {

			/*  Check normal Online nodes */
			// console.log(data.user);

			Device.model.aggregate([

				{
					$match: {
						user: mongoose.Types.ObjectId(data.user),
					},
				},
				{
					$group: {
						_id: null,
						oLevel: {
							$avg: '$oLevel',
						},
						inoLevel: {
							$avg: '$inoLevel',
						},
					},
				},

			], function (err, result) {
				if (err) {
					return next(err);
				} else {
					console.log('[Level Avg Count]');
					// console.log(result);
					if (result.length > 0) {

						binLevels.oLevel = result[0].oLevel || 0;
						binLevels.inoLevel = result[0].inoLevel || 0;
						binLevels.time = Date.now();
					} else {
						console.log('[Level Count - No data found]');
					}

					return next();
				}
			});

		},
		function (next) {

			var msg = ' ';

			var latestData = data.history[0];

			var date = latestData.date;
			var changes = latestData.changes[0];
			// console.log(typeof changes[0],changes[0]);
			var changeType = changes.type;
			var beforeChange = changes.before;
			var afterChange = changes.after;
			var path = changes.path[0];

			console.log(path);

			if (path == 'inoLevel') {

				if (data.inoLevel > 70) {

					notice = true;

					console.log(`Inorganic waste in dustbin ${data.title}  is above limit : ${data.inoLevel}`);
					msg = `Inorganic waste in dustbin ${data.title}  is above limit : ${data.inoLevel} %`;
					socket.to(data.user).emit('notice', {
						type: 'warning',
						data: msg,

					});


					keystone.createItems({

						Alert: [

							{
								alertVal: data.inoLevel,
								alertType: 'inoLevel',
								time: Date.now,
								userKey: data.user,
								binKey: data._id,
							},

						],


					}, {
						verbose: true,
					}, function (err, stats) {
							if (err) next(err);

						});

				}
				if (data.inoLevel < 10) {


					console.log(`Inorganic waste in dustbin ${data.title} is emptied out`);
					msg = `Inorganic waste in dustbin ${data.title} is emptied out`;

					socket.to(data.user).emit('notice', {
						type: 'success',
						data: msg,

					});

				}


			}

			if (path == 'oLevel') {

				if (data.oLevel > 70) {

					notice = true;

					console.log(`Organic waste in dustbin ${data.title} is above limit : ${data.oLevel}`);
					msg = `Organic waste in dustbin ${data.title} is above limit : ${data.oLevel} %`;
					socket.to(data.user).emit('notice', {
						type: 'warning',
						data: msg,

					});

					keystone.createItems({

						Alert: [

							{
								alertVal: data.oLevel,
								alertType: 'oLevel',
								time: Date.now,
								userKey: data.user,
								binKey: data._id,
							},

						],


					}, {
						verbose: true,
					}, function (err, stats) {
							if (err) next(err);
							// return next();
						});

				}

				if (data.oLevel < 10) {

					console.log(`Organic waste in dustbin ${data.title} is emptied out`);

					msg = `Organic waste in dustbin ${data.title} is emptied out`;
					socket.to(data.user).emit('notice', {
						type: 'success',
						data: msg,

					});
				}


			}


			/*  Raise notification for  over */

			// _.each(node_lineup, function (val, key) {

			// 	console.log(data.slug, key, val);
			// 	if (val) {

			// 		if (data.binKey === key) {

			// 			console.log(data.slug);
			// 			return next();

			// 			// socket.to(key).emit('update_count', data[key]);

			// 		}

			// 	// eslint-disable-next-line no-empty
			// 	} else {

			// 		return next();


			// 	}


			// });

			return next();
		},

		function (next) {


			if (notice) {

				var noticeData = data.notice + 1;
				console.log('[-- New alert ---]');
				Device.model.update({ _id: data._id }, {

					notice: noticeData,

				}, function (err, x, y) {

					console.log(y);

					return next();
					
					
				});
				
			}
			else {
				
				return next();

			}


		},
		function (next) {


			var binData = {
				oLevelBin: data.oLevel,
				oLevel: binLevels.oLevel,
				inoLevel: binLevels.inoLevel,
				inoLevelBin: data.inoLevel,
				time: Date.now,
				userKey: data.user,
				binKey: data._id,
			};


			keystone.createItems({

				Levels: [

					binData,

				],


			}, {
				verbose: true,
			}, function (err, stats) {
					if (err) next(err);

					// console.log('our results', stats);

					socket.to(data.user).emit('updated', {
						type: 'levels',
						data: binLevels,

					});
				});


		},

	], function (err) {
		if (err) {

			console.log('[ problem in do count function - socket.js].', err);
			console.log('------------------------------------------------------------');

		}
	});
};


/*
 Function Count
 */

var doCount = function (socket, data, change) {


	async.series([

		// Perform basic validation
		function (next) {

			/*  Check normal Online nodes */

			Device.model.count({
				user: data.user,
				status: 'online',
				state: 'active',
			}, function (err, onCount) {
				if (err) {


					return next(err);

				} else {

					nodeCounts.online = onCount;
					return next();

				}

			});


		},

		// Check for user by email
		function (next) {


			Device.model.count({
				user: data.user,
				status: 'offline',
				state: 'active',
			}, function (err, offline) {
				if (err) {


					return next(err);

				} else {

					nodeCounts.offline = offline;
					return next();

				}

			});


		},

		function (next) {


			Device.model.count({
				user: data.user,
				status: 'faulty',
				state: 'active',
			}, function (err, faulty) {
				if (err) {


					return next(err);

				} else {

					nodeCounts.faulty = faulty;
					return next();

				}

			});


		},
		function (next) {


			keystone.createItems({

				CountNode: [

					{
						offline: nodeCounts.offline,
						online: nodeCounts.online,
						faulty: nodeCounts.faulty,
						none: 0,
						time: Date.now,
						userKey: data.user,
					},

				],


			}, {
				verbose: true,
			}, function (err, stats) {
					if (err) next(err);

					// console.log('our results', stats);

					if (change == 'create') {

						console.log('[doCount : - Created ]');
						// Publish('pingzee:exchange', 'create', 'node', data);

					}
					if (change == 'delete ') {

						console.log('[doCount : - Deleted ]');
						// Publish('pingzee:exchange', 'test', 'node', data);

					} else {

						console.log('[doCount : - Deleted ]');
					}

					socket.to(data.user).emit('updated', {
						type: 'changed',
						data: nodeCounts,

					});
				});


		},

	], function (err) {
		if (err) {

			console.log('[ problem in do count function - socket.js].', err);
			console.log('------------------------------------------------------------');

		}
	});
};


/* Obj of functions for handling commands  */

/*  */


/* New Changed functions ! */

var updates = {

	device: {

		status: function (socket, changes, data) {

			/*  Initial loggging */
			var alert = {
				faulty: {
					class: 'danger',
					lable: 'Faulty',
				},
				online: {
					class: 'success',
					lable: 'Online',
				},
				offline: {
					class: 'warning',
					lable: 'Offline',
				},

			};
			var msg = `<div class="chat-message-left mb-4">
	<div>
		<button type="button" class="btn btn-default btn-round icon-btn ml-1">
			<i class="ion ion-md-happy"></i>
		</button>
		<div class="text-muted small text-nowrap mt-2">${moment().format('hh:mm:ss a')}</div>
	</div>
    <div class="flex-shrink-1 bg-lighter rounded py-2 px-3 ml-3">
        <msg>
        <div class="font-weight-semibold mb-1">Z-Bot</div>       
      <strong>${data.title}</strong> is now  <span class=text-${alert[changes.after].class}>${alert[changes.after].lable}</span>
        </msg>
<hr class="border m-3">
<!-- &nbsp; -->
    </div>
    
</div>`;
			// console.log("status changed to ", changes);
			socket.to(data.user).emit('updated', {
				type: 'status',
				data: msg,
			});

			doCount(socket, data, 'update');


		},

		delete: function (socket, changes, data) {

			/*  Initial loggging */
			console.log('status changed to ', changes);

			// redis.set(`${data.deviceKey}:auth`, 'no');
			// redis.set(`${data.regKey}:auth`, 'no');
			doCount(socket, data, 'delete');


		},
		notice: function (socket, changes, data) {

			/*  Initial loggging */
			console.log('notice changed to ', changes);

			// redis.set(`${data.deviceKey}:auth`, 'no');
			// redis.set(`${data.regKey}:auth`, 'no');
			// doCount(socket, data, 'delete');
			socket.to(data.user).emit('updated', {
				type: 'notice',
			});


		},

		state: function (socket, changes, data) {

			/*  Initial loggging */
			console.log('state changed to ', changes);

			// if (data.state === 'active') {

			// 	redis.set(`${data.deviceKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.deviceKey}:auth`, 'no');
			// 	redis.set(`${data.regKey}:auth`, 'no');

			// }


			doCount(socket, data, 'state');

		},

		createdDate: function (socket, changes, data) {


			/*  Initial loggging */
			// console.log("adding new node ",changes);


			/*  Device Control Auth */

			// if (data.state === 'active') {

			// 	redis.set(`${data.deviceKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.deviceKey}:auth`, 'no');
			// 	redis.set(`${data.regKey}:auth`, 'yes');

			// }

			doCount(socket, data, 'create');


			/*  Counting the node again */


			/*  Sending Data to Exchange - action -test  */


		},
		user: function (socket, changes, data) {


			/*  Initial loggging */
			// console.log("adding new node ",changes);


			/*  Device Control Auth */

			// if (data.state === 'active') {

			// 	redis.set(`${data.deviceKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.deviceKey}:auth`, 'no');
			// 	redis.set(`${data.regKey}:auth`, 'no');

			// }

			doCount(socket, data, 'create');


			/*  Counting the node again */


			/*  Sending Data to Exchange - action -test  */


		},


		oLevel: function (socket, changes, data) {

			/*  Initial loggging */
			// if (data.state === 'active') {

			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.regKey}:auth`, 'no');

			// }

			doLevel(socket, data, 'update');

		},

		inoLevel: function (socket, changes, data) {

			/*  Initial loggging */
			// if (data.state === 'active') {

			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.regKey}:auth`, 'no');

			// }

			doLevel(socket, data, 'update');

		},

		smokeLevel: function (socket, changes, data) {

			/*  Initial loggging */
			// if (data.state === 'active') {

			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.regKey}:auth`, 'no');

			// }

			doLevel(socket, data, 'update');

		},

		aqLevel: function (socket, changes, data) {

			/*  Initial loggging */
			// if (data.state === 'active') {

			// 	redis.set(`${data.regKey}:auth`, 'yes');
			// 	redis.set(`${data.regKey}:key`, data.deviceKey);

			// } else {

			// 	redis.set(`${data.regKey}:auth`, 'no');

			// }

			doLevel(socket, data, 'update');

		},


	},

};


/* --------------Entrance point for the external world Api connections ----------------------*/


var numUsers = 0;

var sock = function (app) {


	console.log('socket server started');


	io = require('socket.io')(app);
	// io.adapter(redisAdapter({
	// 	host: 'localhost',
	// 	port: 6379,
	// }));

	// io.of('/').adapter.allRooms((err, rooms) => {
	// 	console.log(rooms); // an array containing all rooms (accross every node)
	// });
	internal = io.of('/internal');
	ops_io = io.of('/ops');
	page_io = io.of('/page');

	/*  Main IO */

	io.on('connection', function () {

		console.log(' someone connected to main');

	});


	/*  Internal */


	internal.on('connection', function (data) {

		console.log(' someone connected to internal');

	});


	/* Page Presense  Socket  */

	page_io.on('connection', function (socket) {

		console.log('Device Profile Page connected ');


		socket.on('profile', function (data) {

			console.log(data);

			// g(data)

			if (node_lineup[data.node_uid]) {

				console.log('same node profile is opened more than once  ');

				socket.primary = false;
				socket.node_name = data.node_name;
				socket.node_uid = data.node_uid;
				socket.id = data.node_uid;
				socket.join([data.node_uid, 'notice_' + data.node_name, `sandbox-${data.node_uid}`]);

			} else {

				socket.node_name = data.node_name;
				node_lineup[data.node_uid] = true;
				socket.primary = true;
				socket.node_uid = data.node_uid;
				socket.id = data.node_uid;
				console.log(data);
				socket.join([data.node_uid, 'notice_' + data.node_name, `sandbox-${data.node_uid}`]);


			}

		});


		socket.on('analytics', function (data) {

			_.each(node_lineup, function (val, key) {

				if (val) {

					if (typeof (data[key]) !== 'undefined') {

						console.log(data[key]);

						socket.to(key).emit('update_count', data[key]);

					}

				}


			});

		});

		socket.on('disconnect', function () {

			if (socket.primary) {

				delete node_lineup[socket.node_uid];

			}

			console.log('Profile diconnected :', socket.node_name);

		});


	});


	/* Devices List Page */
	ops_io.on('connection', function (socket) {

		console.log('Ops updater connected ');


		socket.on('function', function (data) {

			console.log('Incoming function', data);


			if (data.cmd == 'driver') {


				console.log('Internal Driver connected');


				socket.join('driver');

			}
			5;
			if (data.cmd == 'page') {

				console.log('Ops Page connected');


				socket.join(['page', data.user]);

			}

		});

		socket.on('msgcount', function (data) {
			var totalmsgcount = 0;
			totalmsgcount = data.inmsg + data.outmsg;
			//  g("msg count ", data)
			socket.to('page').emit('totalmsgcount', {
				count: totalmsgcount,
				inmsg: data.inmsg,
				outmsg: data.outmsg,
				custom: data.custom,
			});
			//  exchange.send({ count: totalmsgcount, inmsg: data.inmsg, outmsg: data.outmsg, custom: data.custom }, "totalmsgcount_pipe")

		});

		socket.on('deviceUpdate', function (data) {

			console.log('in Device update [ops]');
			// console.log(data);
			var latestData = data.history[0];

			var date = latestData.date;
			var changes = latestData.changes[0];
			// console.log(typeof changes[0],changes[0]);
			var changeType = changes.type;
			var beforeChange = changes.before;
			var afterChange = changes.after;
			var path = changes.path[0];

			console.log('---->In the Socket---> ', date, changeType, beforeChange, afterChange, path);

			updates.device[path](socket, changes, data);

		});


		socket.on('deviceRemove', function (data) {

			// console.log(data);


			console.log('delete');
			// var latestData = data.history[0]

			// var date = latestData.date ;
			// var changes = latestData.changes[0] ;
			// console.log(typeof changes[0],changes[0]);
			// var changeType  = changes.type ;
			// var beforeChange =changes.before;
			// var afterChange = changes.after;
			// var path =changes.path[0];

			// console.log(date,changeType,beforeChange,afterChange,path);

			updates.device.delete(socket, 'delete', data);

		});

		// socket.on('linkUpdate', function (data) {

		// 	// console.log("in socket");
		// 	var latestData = data.history[0];

		// 	var date = latestData.date;
		// 	var changes = latestData.changes[0];
		// 	// console.log(typeof changes[0],changes[0]);
		// 	var changeType = changes.type;
		// 	var beforeChange = changes.before;
		// 	var afterChange = changes.after;
		// 	var path = changes.path[0];

		// 	console.log('---->In the Link Socket---> ', date, changeType, beforeChange, afterChange, path);

		// 	updates.link[path](socket, changes, data);

		// });


		// socket.on('linkRemove', function (data) {

		// 	// console.log(data);


		// 	console.log('link delete');
		// 	// var latestData = data.history[0]
		// 	updates.link.delete(socket, 'delete', data);

		// });

		// socket.on('pipeUpdate', function (data) {

		// 	// console.log("in socket");
		// 	var latestData = data.history[0];

		// 	var date = latestData.date;
		// 	var changes = latestData.changes[0];
		// 	// console.log(typeof changes[0],changes[0]);
		// 	var changeType = changes.type;
		// 	var beforeChange = changes.before;
		// 	var afterChange = changes.after;
		// 	var path = changes.path[0];

		// 	console.log('---->In the Pipe Socket---> ', date, changeType, beforeChange, afterChange, path);

		// 	updates.pipe[path](socket, changes, data);

		// });


		// socket.on('pipeRemove', function (data) {

		// 	// console.log(data);


		// 	console.log('pipe delete');
		// 	// var latestData = data.history[0]
		// 	updates.pipe.delete(socket, 'delete', data);

		// });


		socket.on('talkBack', function (data) {

			console.log('talkBack', data);


			// Publish('pingzee:exchange', 'talkback', 'node', data);


		});


		// 		socket.on('opsCmd', function (data) {

		// 			// console.log(data);
		// 			// console.log();
		// 			var query = cmdDecode(data.cmd, data.user);
		// 			if (query.success) {

		// 				console.log(query);

		// 				cmdHandler[query.cmd](socket, query);

		// 				// cmdResolve(query, socket);
		// 			} else {

		// 				var msg
		// 					= `<div class="chat-message-left mb-4">
		// 	<div>
		// 		<button type="button" class="btn btn-default btn-round icon-btn ml-1">
		// 			<i class="ion ion-md-happy"></i>
		// 		</button>
		// 		<div class="text-muted small text-nowrap mt-2">${moment().format('hh:mm:ss a')}</div>
		// 	</div>
		//     <div class="flex-shrink-1 bg-lighter rounded py-2 px-3 ml-3">
		//         <msg>
		// 		<div class="font-weight-semibold mb-1">Z-Bot</div>
		// 				${query.comment}
		//         </msg>
		// <hr class="border m-3">
		// <!-- &nbsp; -->

		// </div>
		//     </div>

		// </div>`;
		// 				socket.emit('cmdRes', {
		// 					type: 'tb',
		// 					data: msg,
		// 				});

		// 			}


		// 		});


	});

	/* --------------Sab iske uper karo------------------------*/

	return io;
};


/* --------------------------------------Events end----------------------------------------------------------*/


// /var com = {}

// com.sock = sock;
module.exports = sock;
