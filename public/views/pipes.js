'use strict';
var dev = {};
$(function () {

	/*  */

	/*  Nodes Table */

	dev.tabCol = [{
			"data": `title`
		},
		{
			"data": `pipeKey`
		},
		{
			"data": `createdAt`,
			"render": function (data, type, row) {


				return moment(data).format("MMM Do  hh:mm:ss a")

			}
		},
		{
			"data": `updatedAt`,
			"render": function (data, type, row) {


				return moment(data).format("MMM Do  hh:mm:ss a")

			}
		}
	];



	dev.devTable = $('#pipe-list').DataTable({
		dom: "rtp",
		"processing": false,
		"pageLength": 10,
		"autoWidth": true,
		"serverSide": true,
		"ajax": {

			"url": '/pipes',
			"type": 'POST'
		},
		"columns": dev.tabCol
	});

	// if (Clipboard.isSupported()) {
	// 	console.log("support");

	// 	var clipboard = new Clipboard('button');

	// 	clipboard.on('success', function (e) {
	// 		console.info('Action:', e.action);
	// 		console.info('Text:', e.text);
	// 		console.info('Trigger:', e.trigger);

	// 		e.clearSelection();
	// 	});
	// } else {
	// 	console.log("no support");

	// }



	/* 
		Datepicker

		$('#tickets-list-created').daterangepicker({
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: ($('body').attr('dir') === 'rtl' || $('html').attr('dir') === 'rtl') ? 'right' : 'left'
		});

		// Tooltips

		$('body').tooltip({
			selector: '.product-tooltip'
		}); */

});
