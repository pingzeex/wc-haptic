'use strict';
$(document).ready(function () {

	var profile = {};
	profile.uid = $('#devKey').data('key');
	profile.bid = $('#devID').data('id');
	profile.name = $('#devTitle').data('title');

	profile.nodeHistory = $('#historyNode').DataTable({

		ordering: true,
		pagination: true,
		order: [
			[0, 'desc'],
		],
		// "dom": '<"top" ifr<t>lp><"clear">'
		dom: `rtp`,
	});

	$('#searchbox').on('keyup search input paste cut', function () {
		profile.nodeHistory.search(this.value).draw();
	});


	profile.updateChart = function (start, end, key) {

		
		key = profile.bid ;
		console.log(key);
		// const today = moment().startOf('day').format("DD MMM YYYY")
		// $("#stats_date").text(today);


		// if (nodes.chart.dataProvider.length > 15) {

		//     nodes.chart.dataProvider.shift();
		// }
		// var time = Date.now();

		$.get(`/apis/list/levels?start=${start}&end=${end}&key=${key}`, function (data) {


			// data.data.time = moment(data.data.time).format('hh:mm:ss')

			// console.log(data);
			profile.chart1.dataProvider = data.data;

			profile.chart1.validateData();

		});

	};

	profile.updateChart2 = function (start, end, key) {

		
		key = profile.bid ;
		console.log(key);
		// const today = moment().startOf('day').format("DD MMM YYYY")
		// $("#stats_date").text(today);


		// if (nodes.chart.dataProvider.length > 15) {

		//     nodes.chart.dataProvider.shift();
		// }
		// var time = Date.now();

		$.get(`/apis/list/alerts?start=${start}&end=${end}&key=${key}`, function (data) {

				console.log(data);

			// data.data.time = moment(data.data.time).format('hh:mm:ss')

			// console.log(data);
			profile.chart2.dataProvider = data.data;

			profile.chart2.validateData();

		});

	};
	// Pie charts

	// profile.chart = AmCharts.makeChart("chartdiv", {
	// 	"hideCredits": true,
	// 	"type": "pie",
	// 	"theme": "light",
	// 	"startDuration": 0,
	// 	"innerRadius": 80,
	// 	"pullOutRadius": 20,
	// 	"marginTop": 30,
	// 	"titles": [{
	// 		"text": "Packets Flow Ratio"
	// 	}],
	// 	"allLabels": [{
	// 		"y": "54%",
	// 		"align": "center",
	// 		"size": 25,
	// 		"bold": true,
	// 		"text": `${profile.name}`,
	// 		"color": "#555"
	// 	}, {
	// 		"y": "49%",
	// 		"align": "center",
	// 		"size": 15,
	// 		"text": "Flow",
	// 		"color": "#555"
	// 	}],
	// 	"dataProvider": [{
	// 		"flow": "P2P Tx",
	// 		"packets": 1
	// 	}, {
	// 		"flow": "P2P Rx",
	// 		"packets": 1
	// 	}, {
	// 		"flow": "Echo Tx",
	// 		"packets": 1
	// 	}, {
	// 		"flow": "Echo Rx",
	// 		"packets": 1
	// 	}, {
	// 		"flow": "Pipe Tx",
	// 		"packets": 1
	// 	}, {
	// 		"flow": "Pipe Rx",
	// 		"packets": 1
	// 	}, {
	// 		"flow": "Offline Tx",
	// 		"packets": 1
	// 	}],
	// 	"valueField": "packets",
	// 	"titleField": "flow",
	// 	"balloon": {
	// 		"fixedPosition": true
	// 	},

	// 	"export": {
	// 		"enabled": true
	// 	}


	// });

	/*  Date picker */

	/*  Running Update chart once  */


	/*  Date pickers  */


	profile.start_d = moment().subtract(1, 'days');
	profile.end_d = moment();

	profile.cb = function (start, end) {
		console.log(start.format('MM-DD-YYYY'), end.format('MM-DD-YYYY'));

		profile.updateChart(start.format('MM-DD-YYYY'), end.format('MM-DD-YYYY'),profile.id);
		$('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	};

	$('#daterange-4').daterangepicker({
		startDate: profile.start_d,
		endDate: profile.end_d,
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
		},
		// opens: (isRtl ? 'left' : 'right'),
	}, profile.cb);

	profile.cb(profile.start_d, profile.end_d,profile.bid);


	profile.cb1 = function (start, end) {
		console.log(start.format('MM-DD-YYYY'), end.format('MM-DD-YYYY'));

		profile.updateChart2(start.format('MM-DD-YYYY'), end.format('MM-DD-YYYY'),profile.id);
		$('#daterange-5').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	};

	$('#daterange-5').daterangepicker({
		startDate: profile.start_d,
		endDate: profile.end_d,
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
		},
		// opens: (isRtl ? 'left' : 'right'),
	}, profile.cb1);

	profile.cb1(profile.start_d, profile.end_d,profile.bid);

	/*  Chart update fuction  */

	/*  Chart  */


	profile.chart1 = AmCharts.makeChart('device-chart', {


		type: 'serial',
		// "marginTop": 20,
		// "marginRight": 50,
		// "marginLeft": 20,
		// "marginBottom": 50,
		// "precision": 2,
		autoMargins: true,
		hideCredits: true,
		theme: 'light',
		startDuration: 1,
		// "marginRight": 80,
		// "dateFormat": "DD HH",
		autoMarginOffset: 20,
		legend: {
			useGraphSettings: true,
			position: 'top',
			equalWidths: true,
			valueAlign: 'left',
			valueWidth: 100,
		},
		dataProvider: [{
			time: Date.now(),
			inoLevelBin: 0,
			oLevelBin: 0,
		}],
		// categoryAxis: {
		// 	gridPosition: 'start',
		// 	gridAlpha: 0,
		// 	tickPosition: 'start',
		// 	tickLength: 20,
		// },
		mouseWheelZoomEnabled: true,
		valueAxes: [{
				id: 'g1',
				axisAlpha: 0.5,
				dashLength: 3,
				gridAlpha: 0.5,
				position: 'right',
				autoGridCount: false,
				title: 'Waste Level',
			},
			//      {
			//          "id": "g3",
			//          "axisAlpha": 0,
			//          "dashLength": 3,
			//          "gridAlpha": 0.1,
			//          "position": "left",
			//          "title": "Active Nodes"
			//      }
		],
		graphs: [

			{

				// "bullet": "round",
				// "bulletSize": 10,
				fillAlphas: 0.1,
				lineColor: 'green',
				fillAlphas: 1,
				clustered: true,
				balloonText: `[[value]] % Organic `,
				legendValueText: '[[value]] % ',
				lineThickness: 1.5,
				negativeLineColor: 'green',
				// "type": "smoothedLine",
				type: 'column',
				title: 'Organic ',
				columnWidth: 0.3,
				valueField: 'oLevelBin',
				valueAxis: 'g1',
				dashLengthField: 'dashLengthColumn',

			},
			{

				// "bullet": "round",
				// "bulletSize": 10,
				fillAlphas: 0.1,
				lineColor: 'orange',
				fillAlphas: 1,
				clustered: true,
				balloonText: `[[value]] % Inorganic`,
				legendValueText: '[[value]] % ',
				lineThickness: 1.5,
				negativeLineColor: 'orange',
				// "type": "smoothedLine",
				type: 'column',
				columnWidth: 0.3,
				title: 'Inorganic ',
				valueField: 'inoLevelBin',
				valueAxis: 'g1',

			},

			//     {
			//         "alphaField": "alpha",
			//         "fillColorsField": "color",
			//         "lineAlpha": 0,
			// "balloonText": `[[value]] ${master.gen} `,
			//         "dashLengthField": "dashLengthColumn",
			//         "fillAlphas": 0.3,
			//         "legendValueText": `[[value]] ${master.gen} `,
			//         "title": "Active Nodes",
			//         "type": "column",
			//         "valueField": "nodeCount",
			//         "topRadius": 1,
			//         "valueAxis": "g3"
			//     }
		],
		// "chartScrollbar": {
		// 	"scrollbarHeight": 20,
		// 	"offset": 50,
		// 	"graph": "g1",
		// 	"autoGridCount": true,
		// 	"backgroundAlpha": 0.1,
		// 	"backgroundColor": "#888888",
		// 	"selectedBackgroundColor": "#67b7dc",
		// 	"selectedBackgroundAlpha": 1
		// },
		chartScrollbar: {
			graph: 'g1',
			oppositeAxis: false,
			offset: 30,
			scrollbarHeight: 20,
			backgroundAlpha: 0,
			selectedBackgroundAlpha: 0.1,
			selectedBackgroundColor: '#888888',
			graphFillAlpha: 0,
			graphLineAlpha: 0.5,
			selectedGraphFillAlpha: 0,
			selectedGraphLineAlpha: 1,
			autoGridCount: true,
			color: '#AAAAAA',
		},

		dataDateFormat: 'YYYY-MM-DD HH:NN:SS',
		groupToPeriods: ['ss', '10ss', '30ss', 'mm', '10mm', '30mm', 'hh', 'DD', 'WW', 'MM', 'YYYY'],
		chartCursor: {
			categoryBalloonDateFormat: 'HH:NN:SS, DD MMMM',
			cursorAlpha: 0,
			valueLineEnabled: false,
			valueLineBalloonEnabled: true,
			valueLineAlpha: false,
			color: '#fff',
			cursorColor: '#24abf2',
			fullWidth: false,
		},
		categoryField: 'time',

		categoryAxis: {
			// minHorizontalGap: 10,
			startOnAxis: true,
			showFirstLabel: true,
			showLastLabel: true,
			parseDates: true,
			dashLength: 1,
			minPeriod: 'ss',
			gridAlpha: 0.5,
			axisAlpha: 0,
			fillAlpha: 1,

			autoGridCount: true,
		},
		export: {
			enabled: true,
		},
	});

	/*  Chart  */


	profile.chart2 = AmCharts.makeChart('device-chart1', {


		type: 'serial',
		// "marginTop": 20,
		// "marginRight": 50,
		// "marginLeft": 20,
		// "marginBottom": 50,
		// "precision": 2,
		autoMargins: true,
		hideCredits: true,
		theme: 'light',
		startDuration: 1,
		// "marginRight": 80,
		// "dateFormat": "DD HH",
		autoMarginOffset: 20,
		legend: {
			useGraphSettings: true,
			position: 'top',
			equalWidths: true,
			valueAlign: 'left',
			valueWidth: 100,
		},
		dataProvider: [{
			time: Date.now(),
			alertVal: 0,
		}],
		// categoryAxis: {
		// 	gridPosition: 'start',
		// 	gridAlpha: 0,
		// 	tickPosition: 'start',
		// 	tickLength: 20,
		// },
		mouseWheelZoomEnabled: true,
		valueAxes: [{
				id: 'g1',
				axisAlpha: 0.5,
				dashLength: 3,
				gridAlpha: 0.5,
				position: 'right',
				autoGridCount: false,
				title: 'Alerts',
			},
			//      {
			//          "id": "g3",
			//          "axisAlpha": 0,
			//          "dashLength": 3,
			//          "gridAlpha": 0.1,
			//          "position": "left",
			//          "title": "Active Nodes"
			//      }
		],
		graphs: [

			{

				// "bullet": "round",
				// "bulletSize": 10,
				fillAlphas: 0.1,
				lineColor: 'red',
				fillAlphas: 1,
				clustered: true,
				balloonText: `[[value]] % fill `,
				legendValueText: '[[value]] % fill  ',
				lineThickness: 1.5,
				negativeLineColor: 'red',
				// "type": "smoothedLine",
				type: 'column',
				title: 'Alerts ',
				columnWidth: 0.3,
				valueField: 'alertVal',
				valueAxis: 'g1',
				dashLengthField: 'dashLengthColumn',

			},

			//     {
			//         "alphaField": "alpha",
			//         "fillColorsField": "color",
			//         "lineAlpha": 0,
			// "balloonText": `[[value]] ${master.gen} `,
			//         "dashLengthField": "dashLengthColumn",
			//         "fillAlphas": 0.3,
			//         "legendValueText": `[[value]] ${master.gen} `,
			//         "title": "Active Nodes",
			//         "type": "column",
			//         "valueField": "nodeCount",
			//         "topRadius": 1,
			//         "valueAxis": "g3"
			//     }
		],
		// "chartScrollbar": {
		// 	"scrollbarHeight": 20,
		// 	"offset": 50,
		// 	"graph": "g1",
		// 	"autoGridCount": true,
		// 	"backgroundAlpha": 0.1,
		// 	"backgroundColor": "#888888",
		// 	"selectedBackgroundColor": "#67b7dc",
		// 	"selectedBackgroundAlpha": 1
		// },
		chartScrollbar: {
			graph: 'g1',
			oppositeAxis: false,
			offset: 30,
			scrollbarHeight: 20,
			backgroundAlpha: 0,
			selectedBackgroundAlpha: 0.1,
			selectedBackgroundColor: '#888888',
			graphFillAlpha: 0,
			graphLineAlpha: 0.5,
			selectedGraphFillAlpha: 0,
			selectedGraphLineAlpha: 1,
			autoGridCount: true,
			color: '#AAAAAA',
		},

		dataDateFormat: 'YYYY-MM-DD HH:NN:SS',
		groupToPeriods: ['ss', '10ss', '30ss', 'mm', '10mm', '30mm', 'hh', 'DD', 'WW', 'MM', 'YYYY'],
		chartCursor: {
			categoryBalloonDateFormat: 'HH:NN:SS, DD MMMM',
			cursorAlpha: 0,
			valueLineEnabled: false,
			valueLineBalloonEnabled: true,
			valueLineAlpha: false,
			color: '#fff',
			cursorColor: '#24abf2',
			fullWidth: false,
		},
		categoryField: 'time',

		categoryAxis: {
			// minHorizontalGap: 10,
			startOnAxis: true,
			showFirstLabel: true,
			showLastLabel: true,
			parseDates: true,
			dashLength: 1,
			minPeriod: 'ss',
			gridAlpha: 0.5,
			axisAlpha: 0,
			fillAlpha: 1,

			autoGridCount: true,
		},
		export: {
			enabled: true,
		},
	});

	/* --/--Amcharts */


	/*  JSON Editor */



	/*  Socket IO */


	profile.socket = io.connect('/page');

	profile.socket.on('connect', function () {

		profile.socket.emit('profile', {
			node_uid: profile.uid,
			node_name: profile.name,
		});

	});




	// profile.socket.on('update_count', function (data) {

	// 	// console.log(data);


	// 	var tx = typeof data.tx === 'undefined' ? 0 : data.tx.count;
	// 	var tx_p2p = typeof data.p2p_tx === 'undefined' ? 0 : data.p2p_tx.count;
	// 	var tx_pipe = typeof data.pipe_tx === 'undefined' ? 0 : data.pipe_tx.count;
	// 	var tx_offline = typeof data.offline_tx === 'undefined' ? 0 : data.offline_tx.count;
	// 	var rx_echo = typeof data.echo_rx === 'undefined' ? 0 : data.echo_rx.count;
	// 	var tx_echo = typeof data.echo_tx === 'undefined' ? 0 : data.echo_tx.count;
	// 	var rx = typeof data.rx === 'undefined' ? 0 : data.rx.count;
	// 	var rx_p2p = typeof data.p2p_rx === 'undefined' ? 0 : data.p2p_rx.count;
	// 	var rx_pipe = typeof data.pipe_rx === 'undefined' ? 0 : data.pipe_rx.count;
	// 	var time = Date.now();
	// 	$('#tx_stream').text(tx);
	// 	$('#rx_stream').text(rx);

	// 	if (profile.chart1.dataProvider.length > 15) {

	// 		profile.chart1.dataProvider.shift();
	// 	}
	// 	profile.chart1.dataProvider.push({

	// 		time: time,
	// 		tx: tx,
	// 		rx: rx,
	// 	});


	// 	profile.pie.animateData([{
	// 		flow: 'P2P Tx',
	// 		packets: parseInt(tx_p2p),
	// 	}, {
	// 		flow: 'P2P Rx',
	// 		packets: parseInt(rx_p2p),
	// 	}, {
	// 		flow: 'Echo Tx',
	// 		packets: parseInt(tx_echo),
	// 	}, {
	// 		flow: 'Echo Rx',
	// 		packets: parseInt(rx_echo),
	// 	}, {
	// 		flow: 'Pipe Tx',
	// 		packets: parseInt(tx_pipe),
	// 	}, {
	// 		flow: 'Pipe Rx',
	// 		packets: parseInt(rx_pipe),
	// 	}, {
	// 		flow: 'Offline Tx',
	// 		packets: parseInt(tx_offline),
	// 	}], {
	// 		duration: 1000,
	// 	});
	// 	profile.chart1.validateData();

	// });


	// 	profile.chart.animateData([{
	// 		"flow": "P2P Tx",
	// 		"packets": parseInt(tx_p2p)
	// 	}, {
	// 		"flow": "P2P Rx",
	// 		"packets": parseInt(rx_p2p)
	// 	}, {
	// 		"flow": "Echo Tx",
	// 		"packets": parseInt(tx_echo)
	// 	}, {
	// 		"flow": "Echo Rx",
	// 		"packets": parseInt(rx_echo)
	// 	}, {
	// 		"flow": "Pipe Tx",
	// 		"packets": parseInt(tx_pipe)
	// 	}, {
	// 		"flow": "Pipe Rx",
	// 		"packets": parseInt(rx_pipe)
	// 	}, {
	// 		"flow": "Offline Tx",
	// 		"packets": parseInt(tx_offline)
	// 	}], {
	// 		duration: 1000
	// 	});


	// });


	/* End of the line */
});
