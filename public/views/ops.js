/* eslint-disable space-infix-ops */
'use strict';
var chatList = new PerfectScrollbar(document.getElementById('ops-inner'));
// var devList = new PerfectScrollbar(document.getElementById('ops-dev-list'));
var ops = {};

ops.tb_status = false;
ops.in_status = false;
ops.tb_ch = null;
ops.tb_data = null;
ops.tb_con = null;
ops.in_con = null;

// $('.dataTables_scrollBody').perfectScrollbar()
// ops.devTable =  ;

ops.notice = function (msg, title, type) {

	toastr[type](msg, title, {
		positionClass: 'toast-top-right',
		closeButton: true,
		progressBar: true,
		preventDuplicates: true,
		newestOnTop: true,
	});

};


$('table').on('click', 'a.nameList', function (d) {
	// console.log(device);
	console.log(d);


	var a;
	var key;

	// var info;

	try {
		a = $(this).closest('a');
		// console.log(a);
		key = $(a).data('key');
		// console.log(key);

		// info = JSON.parse(device);
		$.get(`/apis/bin/info?key=${key}`, function (data) {
			if (data.success) {

				// $(data.bot).prependTo('#ops-inner');
				ops.chatbox(data.bot, 'server', false);
				// chatList.update();

			} else {

				ops.chatbox(data.err, 'Z-Bot', true);

			}

			// alert("Load was performed.");
		});
		// alert(info.name);
	} catch (error) {

		console.log(error);

	}

	// console.log(info);
});


/* --/ */
ops.chatbox = function (msg, from, d) {

	var data;

	if (d) {


		data
			= `<div class="chat-message-right mb-4">
		<div>
		<button type="button" class="btn btn-default btn-round icon-btn mr-1">
		<i class="ion ion-md-code"></i>
		</button>
		<div class="text-muted small text-nowrap mt-2">${moment().format('hh:mm:ss a')} </div>
		</div>
		<div class="flex-shrink-1 bg-lighter rounded py-2 px-3 mr-3">
        <msg>
        <div class="font-weight-semibold mb-1">${from}</div>
        ${msg}
        </msg>
		<hr class="border m-3">
		<!-- &nbsp; -->
		</div>
		</div>
		
		</div>`;

	} else {

		data = msg;
	}

	var messagesContainer = $('#ops-inner');
	// var userInput = $('#commands');

	// $(data).prependTo();
	// $('input').val('');

	messagesContainer.append(data);

	// clean out old message
	// userInput.val('');
	// focus on input
	// userInput.focus();

	chatList.update();
	messagesContainer.finish().animate({
		scrollTop: messagesContainer.prop('scrollHeight'),
	}, 250);


};
ops.alertbox = function (msg, from, d) {

	// var data;

	if (d) {

		var str = ' ';
		_.each(msg, function (_val, _key) {
			// console.log(_val,_key);
			str = str + `<li>${(_val.alertType === 'oLevel' ? 'Organic Level' : 'Inorganic Level')} : ${_val.alertVal}  </li>`;

		});
		console.log(str);
	}

	var data
		= `<div class="chat-message-right mb-4">
		<div>
		<button type="button" class="btn btn-danger btn-round icon-btn mr-1">
		<i class="ion ion-md-alert"></i>
		</button>
		<div class="text-muted small text-nowrap mt-2">${moment().format('hh:mm:ss a')} </div>
		</div>
		<div class="flex-shrink-1 bg-lighter rounded py-2 px-3 mr-3">
        <msg>
		<div class="font-weight-semibold mb-1">Alerts : ${from}</div>
		<ul>
		${str}
		</ul>
        </msg>
		<hr class="border m-3">
		<!-- &nbsp; -->
		</div>
		</div>
		
		</div>`;

	// } else {

	// 	data = msg;
	// }

	var messagesContainer = $('#ops-inner');
	// var userInput = $('#commands');

	// $(data).prependTo();
	// $('input').val('');

	messagesContainer.append(data);

	// clean out old message
	// userInput.val('');
	// focus on input
	// userInput.focus();

	chatList.update();
	messagesContainer.finish().animate({
		scrollTop: messagesContainer.prop('scrollHeight'),
	}, 250);


};

/*
function sendNewMessage() {
	var userInput = $('.text-box');
	var newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');

	if (!newMessage) return;

	var messagesContainer = $('.messages');

	messagesContainer.append([
		'<li class="self">',
		newMessage,
		'</li>'
	].join(''));

	// clean out old message
	userInput.html('');
	// focus on input
	userInput.focus();

	messagesContainer.finish().animate({
		scrollTop: messagesContainer.prop("scrollHeight")
	}, 250);
}
 */


$(function () {

	var isRtl = $('body').attr('dir') === 'rtl' || $('html').attr('dir') === 'rtl';
	$('.chat-scroll').each(function () {
		new PerfectScrollbar(this, {
			suppressScrollX: true,
			wheelPropagation: true,
		});
	});
	$('.table-responsive').each(function () {
		new PerfectScrollbar(this, {
			suppressScrollX: false,
			wheelPropagation: true,
		});
	});

	$('.chat-sidebox-toggler').click(function (e) {
		e.preventDefault();
		$('.chat-wrapper').toggleClass('chat-sidebox-open');
	});


	/*  Device Tables for latest */
	ops.tabCol


		= [

			{
				data: `title`,
				render: function (data, type, row) {


					return `<a href="#" data-name="${data}" class="con_name">${data}</a>`;

				},
			},
			{
				data: `binKey`,
				render: function (data, type, row) {


					return `<a href="#" data-key="${data}" class="con_key">${data}</a>`;

				},
			},
			{
				data: `notice`,
				render: function (data, type, row) {


					return '<button type="button" rel="tooltip" title="" class="btn btn-primary btn-sm">' + (data).toString() + ' alerts' + '</button>';

				},
			},
			{
				data: `oLevel`,
				render: function (data, type, row) {
					// eslint-disable-next-line eqeqeq
					if (data === 0) {

						return '<button type="button"  title="" class="btn  btn-sm" style="color:#405d27" >' + (data).toString() + ' %' + '</button>';

					}
					if (data > 0 && data <= 20) {

						return '<button type="button"  title="" class="btn btn-sm" style="color:#405d27" >' + (data).toString() + ' %' + '</button>';

					}
					if (data > 20 && data <= 40) {

						return '<button type="button" rel="tooltip" title="" class="btn btn-success btn-sm">' + (data).toString() + ' %' + '</button>';

					}

					if (data > 40 && data <= 60) {

						return '<button type="button" rel="tooltip" title="" class="btn  btn-sm btn-warning"  >' + (data).toString() + ' %' + '</button>';

					}

					if (data > 60 && data <= 80) {

						// ops.notice(`Organic Bin level reached to ${data}`, 'Waste Warning', 'warning');

						return '<button type="button" rel="tooltip" title="" class="btn  btn-sm" style="background-color:orange;color:white;" >' + (data).toString() + ' %' + '</button>';

					}

					if (data > 80 && data <= 100) {

						return '<button type="button" rel="tooltip" title="" class="btn btn-danger btn-sm">' + (data).toString() + ' %' + '</button>';

					}

				},


			},
			{
				data: `inoLevel`,
				render: function (data, type, row) {
					// eslint-disable-next-line eqeqeq
					if (data === 0) {

						return '<button type="button"  title="" class="btn  btn-sm" style="color:#405d27" >' + (data).toString() + ' %' + '</button>';

					}
					if (data > 0 && data <= 20) {

						return '<button type="button"  title="" class="btn btn-sm" style="color:#405d27" >' + (data).toString() + ' %' + '</button>';

					}
					if (data > 20 && data <= 40) {

						return '<button type="button" rel="tooltip" title="" class="btn btn-success btn-sm">' + (data).toString() + ' %' + '</button>';

					}

					if (data > 40 && data <= 60) {

						return '<button type="button" rel="tooltip" title="" class="btn  btn-sm btn-warning"  >' + (data).toString() + ' %' + '</button>';

					}

					if (data > 60 && data <= 80) {

						// ops.notice(`Inorganic Bin level reached to ${data}`, 'Waste Warning', 'warning');

						return '<button type="button" rel="tooltip" title="" class="btn  btn-sm"  style="background-color:orange;color:white;" >' + (data).toString() + ' %' + '</button>';

					}

					if (data > 80 && data <= 100) {

						return '<button type="button" rel="tooltip" title="" class="btn btn-danger btn-sm">' + (data).toString() + ' %' + '</button>';

					}

				},


			},
			{
				data: `status`,
				render: function (data, type, row) {
					// eslint-disable-next-line eqeqeq
					if (data == 'online') {

						return '<button type="button"  title="" class="btn btn-success btn-sm">Online</button>';

					}
					if (data == 'offline') {

						return '<button type="button" rel="tooltip" title="" class="btn btn-warning btn-sm">Offline</button>';

					}

					if (data == 'faulty') {

						return '<button type="button" rel="tooltip" title="" class="btn btn-danger btn-sm">Faulty</button>';

					}

					if (data == 'none') {

						return '<button type="button" rel="tooltip" title="" class="btn btn-primary ">Not In use</button>';

					}

				},


			},
			{
				data: `binKey`,
				render: function (data, type, row) {

					return `<a  href="/bin/${data}" class="btn btn-outline-primary btn-sm">View</a>`;


				},


			},
		];


	ops.devicesTable = $('#device-list').DataTable({
		dom: 'rtp',
		processing: false,
		select: true,
		pageLength: 10,
		autoWidth: true,
		serverSide: true,
		ajax: {

			url: '/bins',
			type: 'POST',
		},
		columns: ops.tabCol,
	});


	/*  Add connection handler */


	/*  Amchart function */

	ops.updateChart = function (start, end) {

		// console.log("working");
		// const today = moment().startOf('day').format("DD MMM YYYY")
		// $("#stats_date").text(today);


		// if (nodes.chart.dataProvider.length > 15) {

		//     nodes.chart.dataProvider.shift();
		// }
		// var time = Date.now();

		$.get(`/apis/list/levels?start=${start}&end=${end}`, function (data) {


			// data.data.time = moment(data.data.time).format('hh:mm:ss')

			// console.log(data);
			ops.chart.dataProvider = data.data;

			ops.chart.validateData();

		});

	};


	/*  Running Update chart once  */


	/*  Date pickers  */


	ops.start_d = moment().subtract(1, 'days');
	ops.end_d = moment();

	ops.cb = function (start, end) {
		console.log(start.format('MM-DD-YYYY'), end.format('MM-DD-YYYY'));

		ops.updateChart(start.format('MM-DD-YYYY'), end.format('MM-DD-YYYY'));
		$('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	};

	$('#daterange-4').daterangepicker({
		startDate: ops.start_d,
		endDate: ops.end_d,
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
		},
		opens: (isRtl ? 'left' : 'right'),
	}, ops.cb);

	ops.cb(ops.start_d, ops.end_d);


	/* --/-- */


	$('#commands').attr('autocomplete', 'off');
	// ops.devTable = $('#ops-dev-table').DataTable({
	// 	"scrollY": "400px",
	// 	"initComplete": function () {

	// 		// $('.dataTables_scrollBody').each(function () {
	// 		// 			new PerfectScrollbar(this, {
	// 		// 				suppressScrollX: true,
	// 		// 				wheelPropagation: true
	// 		// 			});
	// 		$('.dataTables_scrollBody').each(function () {
	// 			new PerfectScrollbar(this, {
	// 				suppressScrollX: true,
	// 				wheelPropagation: true
	// 			});
	// 		});
	// 	},
	// 	"scrollCollapse": true,
	// 	"dom": `rt`,
	// 	"ajax": "/apis/list/devices",
	// 	"columns": [{
	// 		"data": "device"
	// 	}, ]
	// });


	// $("#searchbox").on("keyup search input paste cut", function () {
	// 	ops.devTable.search(this.value).draw();
	// });


	// console.log("its the page");


	/* ======Command  Console handler ====== */


	$('#ops-chat').on('submit', function (event) {
		event.preventDefault();
		var message = $('#commands').val();
		console.log(message);

		if (ops.tb_status) {
			if (ops.tb_ch !== null) {

				if (message === 'tb-exit') {
					ops.tb_status = false;
					ops.tb_ch = null;
					ops.tb_con = null;
					ops.chatbox('Tb Closed', 'Talk-Back', true);

				} else if (message === 'tb-ch') {
					ops.tb_ch = null;
					ops.chatbox('Enter a new channel', 'Talk-Back', true);

				} else {

					ops.io.emit('opsCmd', {
						cmd: '//tb-' + ops.tb_con + '-' + ops.tb_ch + '-' + message,
						user: $('#randomKey').data('key'),
					});
					// ops.chatbox(message + " channel is selected", "Talk-Back", true);

				}


			} else if (ops.tb_ch === null) {

				ops.tb_ch = message;
				ops.chatbox(message + ' channel is selected, send your data ', 'Talk-Back', true);

			}

		} else {

			ops.io.emit('opsCmd', {
				cmd: message,
				user: $('#randomKey').data('key'),
			});

			ops.chatbox(message, 'User', true);
		}
	});


	/*  Stand Table check system  */

	$('.tabSelect').click(function () {

		var status = $(this).data('status');
		if (status == 'total') {
			ops.devicesTable.ajax.url(`/bins`).load();
		} else {
			ops.devicesTable.ajax.url(`/bins?status=${status}`).load();

		}

	});


	/*  Amchats */


	ops.chart = AmCharts.makeChart('statistics-chart-17', {


		type: 'serial',
		// "marginTop": 20,
		// "marginRight": 50,
		// "marginLeft": 20,
		// "marginBottom": 50,
		// "precision": 2,
		autoMargins: true,
		hideCredits: true,
		theme: 'light',
		startDuration: 1,
		// "marginRight": 80,
		// "dateFormat": "DD HH",
		autoMarginOffset: 20,
		legend: {
			useGraphSettings: true,
			position: 'top',
			equalWidths: true,
			valueAlign: 'left',
			valueWidth: 100,
		},
		dataProvider: [{
			time: Date.now(),
			inoLevel: 0,
			oLevel: 0,
		}],
		// categoryAxis: {
		// 	gridPosition: 'start',
		// 	gridAlpha: 0,
		// 	tickPosition: 'start',
		// 	tickLength: 20,
		// },
		mouseWheelZoomEnabled: true,
		valueAxes: [{
			id: 'g1',
			axisAlpha: 0.5,
			dashLength: 3,
			gridAlpha: 0.5,
			position: 'right',
			autoGridCount: false,
			title: 'Waste Level',
		},
			//      {
			//          "id": "g3",
			//          "axisAlpha": 0,
			//          "dashLength": 3,
			//          "gridAlpha": 0.1,
			//          "position": "left",
			//          "title": "Active Nodes"
			//      }
		],
		graphs: [

			{

				// "bullet": "round",
				// "bulletSize": 10,
				fillAlphas: 0.1,
				lineColor: 'green',
				fillAlphas: 1,
				clustered: true,
				balloonText: `[[value]] % Organic `,
				legendValueText: '[[value]] % ',
				lineThickness: 1.5,
				negativeLineColor: 'green',
				// "type": "smoothedLine",
				type: 'column',
				title: 'Organic ',
				columnWidth: 0.3,
				valueField: 'oLevel',
				valueAxis: 'g1',
				dashLengthField: 'dashLengthColumn',

			},
			{

				// "bullet": "round",
				// "bulletSize": 10,
				fillAlphas: 0.1,
				lineColor: 'orange',
				fillAlphas: 1,
				clustered: true,
				balloonText: `[[value]] % Inorganic`,
				legendValueText: '[[value]] % ',
				lineThickness: 1.5,
				negativeLineColor: 'orange',
				// "type": "smoothedLine",
				type: 'column',
				columnWidth: 0.3,
				title: 'Inorganic ',
				valueField: 'inoLevel',
				valueAxis: 'g1',

			},

			//     {
			//         "alphaField": "alpha",
			//         "fillColorsField": "color",
			//         "lineAlpha": 0,
			// "balloonText": `[[value]] ${master.gen} `,
			//         "dashLengthField": "dashLengthColumn",
			//         "fillAlphas": 0.3,
			//         "legendValueText": `[[value]] ${master.gen} `,
			//         "title": "Active Nodes",
			//         "type": "column",
			//         "valueField": "nodeCount",
			//         "topRadius": 1,
			//         "valueAxis": "g3"
			//     }
		],
		// "chartScrollbar": {
		// 	"scrollbarHeight": 20,
		// 	"offset": 50,
		// 	"graph": "g1",
		// 	"autoGridCount": true,
		// 	"backgroundAlpha": 0.1,
		// 	"backgroundColor": "#888888",
		// 	"selectedBackgroundColor": "#67b7dc",
		// 	"selectedBackgroundAlpha": 1
		// },
		chartScrollbar: {
			graph: 'g1',
			oppositeAxis: false,
			offset: 30,
			scrollbarHeight: 20,
			backgroundAlpha: 0,
			selectedBackgroundAlpha: 0.1,
			selectedBackgroundColor: '#888888',
			graphFillAlpha: 0,
			graphLineAlpha: 0.5,
			selectedGraphFillAlpha: 0,
			selectedGraphLineAlpha: 1,
			autoGridCount: true,
			color: '#AAAAAA',
		},

		dataDateFormat: 'YYYY-MM-DD HH:NN:SS',
		groupToPeriods: ['ss', '10ss', '30ss', 'mm', '10mm', '30mm', 'hh', 'DD', 'WW', 'MM', 'YYYY'],
		chartCursor: {
			categoryBalloonDateFormat: 'HH:NN:SS, DD MMMM',
			cursorAlpha: 0,
			valueLineEnabled: false,
			valueLineBalloonEnabled: true,
			valueLineAlpha: false,
			color: '#fff',
			cursorColor: '#24abf2',
			fullWidth: false,
		},
		categoryField: 'time',

		categoryAxis: {
			// minHorizontalGap: 10,
			startOnAxis: true,
			showFirstLabel: true,
			showLastLabel: true,
			parseDates: true,
			dashLength: 1,
			minPeriod: 'hh',
			gridAlpha: 0.5,
			axisAlpha: 0,
			fillAlpha: 1,

			autoGridCount: true,
		},
		export: {
			enabled: true,
		},
	});

	/* --/--Amcharts */

	/* Connection Table link */
	$('table.connections').on('click', '.con_name', function (e) {

		e.preventDefault();
		var data = {};
		var connection_name = $(this).attr('data-name');
		$.get(`/apis/bin/info?title=${connection_name}`, function (data) {
			if (data.success) {
				ops.chatbox(data.bot, 'server', false);

				if (data.data.alert) {

					ops.alertbox(data.data.notice,data.data.title, true);

				}
			} else {

				ops.chatbox(data.err, 'Z-Bot', true);

			}

		});

	});

	$('table.connections').on('click', '.con_key', function (e) {
		e.preventDefault();

		var data = {};
		var connection_key = $(this).attr('data-key');

		var dummy = document.createElement('input');
		// dummy.style.display = "none"
		document.body.appendChild(dummy);
		dummy.setAttribute('value', connection_key);
		dummy.select();
		document.execCommand('copy');
		document.body.removeChild(dummy);
		var msg = 'Key copied : ' + connection_key;
		var title = 'Notification';
		var type = 'success';
		/*
		 For getting the checked element's value
		$('input[name="toastr-position"]:checked').val() */
		toastr[type](msg, title, {
			positionClass: 'toast-top-right',
			closeButton: true,
			progressBar: true,
			preventDuplicates: true,
			newestOnTop: true,
		});

		console.log(connection_key);


	});


	/*  Socket.io */


	ops.io = io('/ops');
	ops.io.on('connect', function () {
		$('.sockStatus').text('Online');
		// alert("connected")
		ops.io.emit('function', {
			cmd: 'page',
			user: $('#randomKey').data('key'),
		});
	});
	ops.io.on('disconnect', function () {
		$('.sockStatus').text('Offline');
		// alert("connected")
		// ops.io.emit('function', {
		// 	cmd: 'page',
		// 	user: $("#randomKey").data('key')
		// });
	});

	ops.io.on('updated', function (data) {

		ops.cb(ops.start_d, ops.end_d);

		if (data.type == 'status') {

			console.log('something updated');
			ops.chatbox(data.data, 'server', false);
			// $(data.data).prependTo('#ops-inner');
			// ops.devTable.ajax.reload();
			ops.devicesTable.ajax.reload();
			// $('input').val('');
			// chatList.update();


			// updateChart()
		}

		if (data.type == 'changed') {

			// ops.devTable.ajax.reload();
			ops.devicesTable.ajax.reload();
			console.log('[Connection count changed]');

			// console.log(data.data);
			var count = data.data;
			var ttl = parseInt(count.offline) + parseInt(count.online) + parseInt(count.faulty);
			$('#totalDev').text(ttl.toString());
			$('#onlineDev').text(count.online);
			$('#offlineDev').text(count.offline);
			$('#faultyDev').text(count.faulty);
			// $('#echo_stream').text(data.custom.echoCount);
			// $('#p2p_stream').text(data.custom.p2pCount);
			// $('#pipe_stream').text(data.custom.pipeCount);


		}
		if (data.type == 'levels') {

			// ops.devTable.ajax.reload();
			ops.devicesTable.ajax.reload();
			console.log('[Connection Level changed]');
			// console.log(data.data);

			// var count = data.data;
			// var ttl = parseInt(count.offline) + parseInt(count.online) + parseInt(count.faulty);
			// $('#totalDev').text(ttl.toString());
			// $('#onlineDev').text(count.online);
			// $('#offlineDev').text(count.offline);
			// $('#faultyDev').text(count.faulty);
			// $('#echo_stream').text(data.custom.echoCount);
			// $('#p2p_stream').text(data.custom.p2pCount);
			// $('#pipe_stream').text(data.custom.pipeCount);


		}
		if (data.type == 'notice') {

			// ops.devTable.ajax.reload();
			ops.devicesTable.ajax.reload();
			console.log('[Connection Level changed]');
			// console.log(data.data);

			// var count = data.data;
			// var ttl = parseInt(count.offline) + parseInt(count.online) + parseInt(count.faulty);
			// $('#totalDev').text(ttl.toString());
			// $('#onlineDev').text(count.online);
			// $('#offlineDev').text(count.offline);
			// $('#faultyDev').text(count.faulty);
			// $('#echo_stream').text(data.custom.echoCount);
			// $('#p2p_stream').text(data.custom.p2pCount);
			// $('#pipe_stream').text(data.custom.pipeCount);


		}

	});
	ops.io.on('cmdRes', function (data) {


		if (data.type == 'sandbox-link') {


		}
		if (data.type == 'sandbox-unlink') {

		}
		if (data.type == 'sandbox-err') {

		}
		if (data.type == 'tb') {

		}

		// console.log("something updated");
		ops.chatbox(data.data, 'server', false);
		// $(data.data).prependTo('#ops-inner');
		// chatList.update();

	});
	ops.io.on('response', function (data) {

		console.log('[ Somthing came in response emmiter ]');
		// console.log(data);


		// if (data.type == "cmd") {

		//     console.log("something updated");


		// 	// updateChart()
		// }

	});
	ops.io.on('sandbox', function (data) {

		// console.log('sandbox data');
		// console.log(data);
		// ops.editor.update(data);


		// if (data.type == "cmd") {

		//     console.log("something updated");


		// 	// updateChart()
		// }

	});
	ops.io.on('notice', function (data) {

		console.log(data);
		ops.notice(data.data, 'WC-Haptic', data.type);
		// console.log(data);
		// ops.editor.update(data);


		// if (data.type == "cmd") {

		//     console.log("something updated");


		// 	// updateChart()
		// }

	});
	// ops.io.on("totalmsgcount", function (data) {

	// 	// console.log(data);


	// 	if (ops.chart.dataProvider.length > 20) {

	// 		ops.chart.dataProvider.shift();
	// 	}
	// 	var time = Date.now();

	// 	ops.tx_stream_c = parseFloat(data.inmsg);
	// 	ops.rx_stream_c = parseFloat(data.outmsg);
	// 	ops.pipe_stream_c = parseFloat(data.custom.pipeCount);
	// 	ops.p2p_stream_c = parseFloat(data.custom.p2pCount);
	// 	ops.echo_stream_c = parseFloat(data.custom.echoCount);

	// 	/*  For displaying data in upper cells -- data flow rate  */

	// 	// $('#tx_stream').text(data.inmsg);
	// 	// $('#rx_stream').text(data.outmsg);
	// 	// $('#echo_stream').text(data.custom.echoCount);
	// 	// $('#p2p_stream').text(data.custom.p2pCount);
	// 	// $('#pipe_stream').text(data.custom.pipeCount);
	// 	ops.chart.dataProvider.push({
	// 		time: time,

	// 		offmsg: data.custom.offCount,
	// 		echomsg: data.custom.echoCount,
	// 		p2pmsg: data.custom.p2pCount,
	// 		pipemsg: data.custom.pipeCount,

	// 	});
	// 	ops.chart.validateData();


	// })


});
//   });
