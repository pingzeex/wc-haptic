'use strict';
var dev = {};

$(function () {

	/*  */

	/*  Nodes Table */

	dev.tabCol = [{
			"data": `title`
		},
		{ "data": `deviceKey` },
		// {
		// 	"data": `deviceKey`,
		// 	"render": function (data, type, row) {


		// 		return `<div  class="d-flex form-group">
        //               <div style="width:100%" class="input-group input-group-m">
        //                 <input  type="text" value="${data}" readonly class="form-control" placeholder=" ">
        //               </div>
        //             </div>`

		// 	}
		// },
		{
			"data": `createdAt`,
			"render": function (data, type, row) {


				return moment(data).format("MMM Do  hh:mm:ss a")

			}
		},
		{
			"data": `updatedAt`,
			"render": function (data, type, row) {


				return moment(data).format("MMM Do  hh:mm:ss a")

			}
		},
		{
			"data": `categories`,
			"render": function (data, type, row) {

				return data[0].lable

			}
		},
		{
			"data": `status`,
			"render": function (data, type, row) {
				if (data == "online") {

					return '<button type="button"  title="" class="btn btn-outline-success btn-sm">Online</button>'

				}
				if (data == "offline") {

					return '<button type="button" rel="tooltip" title="" class="btn btn-outline-warning btn-sm">Offline</button>'

				}

				if (data == "faulty") {

					return '<button type="button" rel="tooltip" title="" class="btn btn-outline-danger btn-sm">Faulty</button>';

				}

				if (data == "none") {

					return '<button type="button" rel="tooltip" title="" class="btn btn-primary ">Not In use</button>';

				}

			}



		},
		{
			"data": `deviceKey`,
			"render": function (data, type, row) {
				
									return `<a  href="/device/${data}" class="btn btn-outline-primary btn-sm">View</a>`;
		

			}



		}
	];



	dev.devTable = $('#device-list').DataTable({
		dom: "rtp",
		"processing": false,
		"select": true,
		"pageLength": 10,
		"autoWidth": true,
		"serverSide": true,
		"ajax": {

			"url": '/devices',
			"type": 'POST'
		},
		"columns": dev.tabCol
	});

	// if (Clipboard.isSupported()) {
	// 	console.log("support");

	// 	var clipboard = new Clipboard('button');

	// 	clipboard.on('success', function (e) {
	// 		console.info('Action:', e.action);
	// 		console.info('Text:', e.text);
	// 		console.info('Trigger:', e.trigger);

	// 		e.clearSelection();
	// 	});
	// } else {
	// 	console.log("no support");

	// }
  function openSidenav() {
  	$('.clients-wrapper').addClass('clients-sidebox-open');
  }

  function closeSidenav() {
  	$('.clients-wrapper').removeClass('clients-sidebox-open');
  	// $('.clients-table tr.bg-light').removeClass('bg-light');
  }

 $('body').on('click', '.clients-sidebox-close', function (e) {
 	e.preventDefault();
 	closeSidenav();
 });
	$("table").on("click", "tr td:not(:last-child)", function (e) {


		var $tr = $(this).closest('tr');
		var device_data = dev.devTable.row($tr).data();

		// openSidenav()

	
		// console.log(device_data);
		

	});

	/* 
		Datepicker

		$('#tickets-list-created').daterangepicker({
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: ($('body').attr('dir') === 'rtl' || $('html').attr('dir') === 'rtl') ? 'right' : 'left'
		});

		// Tooltips

		$('body').tooltip({
			selector: '.product-tooltip'
		}); */

});
