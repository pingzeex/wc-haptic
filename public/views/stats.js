'use strict';
var stats = {};



/*  Amchats */
stats.chart = AmCharts.makeChart("traffic", {


	"type": "serial",
	"marginTop": 10,
	"hideCredits": true,
	"theme": "light",
	"legend": {
		"equalWidths": true,
		"useGraphSettings": true,
		"valueAlign": "left",
		"valueWidth": 100
	},
	"dataProvider": [{
		"time": Date.now(),
		inmsg: 0,
		outmsg: 0,
		echomsg: 0,
		p2pmsg: 0,
		offmsg: 0,
		pipemsg: 0,
		errmsg: 0,
		"color": "#9c27b0"
	}],
	"valueAxes": [{
		"id": "g1",
		"axisAlpha": 0,
		"dashLength": 3,
		"gridAlpha": 0.1,
		"position": "right",
		"title": "Data/sec"
	}, ],
	"graphs": [

		{

			"bullet": "round",
			"bulletSize": 7,
			"fillAlphas": 0.2,
			//  "lineColor": "green",
			"balloonText": `[[value]] Echo `,
			"legendValueText": "[[value]] /sec",
			"lineThickness": 1.5,
			//   "negativeLineColor": "green",
			"type": "smoothedLine",
			"title": "Echo-Data ",
			// "type": "column",
			"valueField": "echomsg",
			"valueAxis": "g1"

		},
		{

			"bullet": "round",
			"bulletSize": 7,
			"fillAlphas": 0.2,
			//  "lineColor": "green",
			"balloonText": `[[value]] Tx `,
			"legendValueText": "[[value]] /sec",
			"lineThickness": 1.5,
			//   "negativeLineColor": "green",
			"type": "smoothedLine",
			"title": "Tx-Data ",
		
			"valueField": "inmsg",
			"valueAxis": "g1"

		},
		{

			"bullet": "round",
			"bulletSize": 7,
			"fillAlphas": 0.2,
			"balloonText": `[[value]] Rx `,
			"legendValueText": "[[value]] /sec",
			"lineThickness": 1.5,
			"type": "smoothedLine",
			"title": "Rx-Data ",
			"valueField": "outmsg",
			"valueAxis": "g1"

		},
		{

			"bullet": "round",
			"bulletSize": 7,
			"fillAlphas": 0.2,
			//   "lineColor": "green",
			"balloonText": `[[value]] Piped `,
			"legendValueText": "[[value]] /sec",
			"lineThickness": 1.5,
			// "negativeLineColor": "green",
			"type": "smoothedLine",
			"title": "Pipe-Data ",
			// "type": "column",
			"valueField": "pipemsg",
			"valueAxis": "g1"

		},
		{

			"bullet": "round",
			"bulletSize": 7,
			"fillAlphas": 0.2,
			//    "lineColor": "green",
			"balloonText": `[[value]] P2P `,
			"legendValueText": "[[value]] /sec",
			"lineThickness": 1.5,
			//    "negativeLineColor": "green",
			"type": "smoothedLine",
			"title": "P2P-Data ",
			// "type": "column",
			"valueField": "p2pmsg",
			"valueAxis": "g1"

		},
		{

			"bullet": "round",
			"bulletSize": 7,
			"fillAlphas": 0.2,
			//  "lineColor": "green",
			"balloonText": `[[value]] Offline `,
			"legendValueText": "[[value]] /sec",
			"lineThickness": 1.5,
			// "negativeLineColor": "green",
			"type": "smoothedLine",
			"title": "Offline-Data ",
			// "type": "column",
			"valueField": "offmsg",
			"valueAxis": "g1"

		},

	],
	"chartCursor": {
		"cursorAlpha": 0,
		"valueLineEnabled": false,
		"valueLineBalloonEnabled": true,
		"valueLineAlpha": false,
		"color": '#fff',
		"cursorColor": '#FC6180',
		"fullWidth": true
	},
	//"depth3D": 20,
	// "angle": 30,
	"categoryField": "time",
	"categoryAxis": {
		"parseDates": true,
		"minPeriod": "ss",
		"gridAlpha": 0,
		"axisAlpha": 0,
		"fillAlpha": 0,
		//  "fillColor": "#FAFAFA",
		"minorGridAlpha": 0,
		"autoGridCount": true,
		// "minorGridEnabled": true
	},
	"export": {
		"enabled": true
	}
});



	/*  Sparkling charts */
	stats.tx_chart = new Chart(document.getElementById('statistics-chart-tx').getContext("2d"), {
		type: 'line',
		data: {
			datasets: [{
				data: [0],
				borderWidth: 1,
				backgroundColor: 'rgba(0,0,0,0)',
				borderColor: '#8897aa',
				pointBorderColor: 'rgba(0,0,0,0)',
				pointRadius: 1,
				lineTension: 0
			}],
			labels: ['']
		},

		options: {
			scales: {
				xAxes: [{
					display: false,
				}],
				yAxes: [{
					display: false
				}]
			},
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			},
			responsive: true,
			maintainAspectRatio: false
		}
	});

	stats.rx_chart = new Chart(document.getElementById('statistics-chart-rx').getContext("2d"), {
		type: 'line',
		data: {
			datasets: [{
				data: [0],
				borderWidth: 1,
				backgroundColor: 'rgba(0,0,0,0)',
				borderColor: '#673AB7',
				pointBorderColor: 'rgba(0,0,0,0)',
				pointRadius: 1,
				lineTension: 0
			}],
			labels: ['']
		},

		options: {
			scales: {
				xAxes: [{
					display: false,
				}],
				yAxes: [{
					display: false
				}]
			},
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			},
			responsive: true,
			maintainAspectRatio: false 
		}
	});


	stats.p2p_chart = new Chart(document.getElementById('statistics-chart-p2p').getContext("2d"), {
		type: 'line',
		data: {
			datasets: [{
				data: [0],
				borderWidth: 1,
				backgroundColor: 'rgba(0,0,0,0)',
				borderColor: '#009688',
				pointBorderColor: 'rgba(0,0,0,0)',
				pointRadius: 1,
				lineTension: 0
			}],
			labels: ['']
		},

		options: {
			scales: {
				xAxes: [{
					display: false,
				}],
				yAxes: [{
					display: false
				}]
			},
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			},
			responsive: true,
			maintainAspectRatio: false
		}
	});


	stats.pipe_chart = new Chart(document.getElementById('statistics-chart-pipe').getContext("2d"), {
		type: 'line',
		data: {
			datasets: [{
				data: [0],
				borderWidth: 1,
				backgroundColor: 'rgba(0,0,0,0)',
				borderColor: 'rgba(206, 221, 54, 1)',
				pointBorderColor: 'rgba(0,0,0,0)',
				pointRadius: 1,
				lineTension: 0
			}],
			labels: ['']
		},

		options: {
			scales: {
				xAxes: [{
					display: false,
				}],
				yAxes: [{
					display: false
				}]
			},
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			},
			responsive: true,
			maintainAspectRatio: false
		}
	});

    /*  Echo chat */
/* 	stats.echo_chart = new Chart(document.getElementById('statistics-chart-echo').getContext("2d"), {
		type: 'line',
		data: {
			datasets: [{
				data: [0],
				borderWidth: 1,
				backgroundColor: 'rgba(0,0,0,0)',
				borderColor: '#26B4FF',
				pointBorderColor: 'rgba(0,0,0,0)',
				pointRadius: 1,
				lineTension: 0
			}],
			labels: ['']
		},

		options: {
			scales: {
				xAxes: [{
					display: false,
				}],
				yAxes: [{
					display: false
				}]
			},
			legend: {
				display: false
			},
			tooltips: {
				enabled: false
			},
			responsive: true,
			maintainAspectRatio: false
		}
	});
 */






	/* Chart functions  */
	setInterval(function () {

		if (stats.tx_chart.data.datasets[0].data.length > 20) {

				stats.tx_chart.data.datasets[0].data.shift();
                stats.tx_chart.data.labels.shift();
                stats.tx_chart.update()
		}

		if (stats.rx_chart.data.datasets[0].data.length > 20) {

                stats.rx_chart.data.datasets[0].data.shift();
                stats.rx_chart.data.labels.shift()
                stats.rx_chart.update()
		}
		if (stats.pipe_chart.data.datasets[0].data.length > 20) {

                stats.pipe_chart.data.datasets[0].data.shift();
                	stats.p2p_chart.data.labels.shift()
                	stats.p2p_chart.update()
		}
		if (stats.p2p_chart.data.datasets[0].data.length > 20) {

                stats.p2p_chart.data.datasets[0].data.shift();
                stats.pipe_chart.data.labels.shift()
                stats.pipe_chart.update()

        }
        


/* 		if (stats.echo_chart.data.datasets[0].data.length > 30) {

				stats.echo_chart.data.datasets[0].data.shift();
		}
 */


		stats.tx_chart.data.datasets[0].data.push(stats.tx_stream_c || 0 )
		stats.tx_chart.data.labels.push('')
		stats.tx_chart.update()

		stats.rx_chart.data.datasets[0].data.push(stats.rx_stream_c || 0)
		stats.rx_chart.data.labels.push('')
		stats.rx_chart.update()

		stats.p2p_chart.data.datasets[0].data.push(stats.p2p_stream_c || 0)
		stats.p2p_chart.data.labels.push('')
		stats.p2p_chart.update()

		stats.pipe_chart.data.datasets[0].data.push(stats.pipe_stream_c || 0)
		stats.pipe_chart.data.labels.push('')
		stats.pipe_chart.update()

		// stats.echo_chart.data.datasets[0].data.push(stats.echo_stream_c || 0)
		// stats.echo_chart.data.labels.push('')
		// stats.echo_chart.update()

        
	}, 5000)


	/* ---/-- Sparkling charts */

/*  Socket.io */


stats.io = io('/ops');
stats.io.on('connect', function () {
	// $(".sockStatus").text("Online");
	// alert("connected")
	stats.io.emit('function', {
		cmd: 'page',
		user: $("#randomKey").data('key')
	});
});
stats.io.on('disconnect', function () {
	// $(".sockStatus").text("Offline")
	// alert("connected")
	// stats.io.emit('function', {
	// 	cmd: 'page',
	// 	user: $("#randomKey").data('key')
	// });
});

stats.io.on('updated', function (data) {



	if (data.type == "status") {

		console.log("something updated");
		// stats.chatbox(data.data, 'server', false);
		// $(data.data).prependTo('#stats-inner');
		// stats.devTable.ajax.reload();
		// $('input').val('');
		// chatList.update();


		// updateChart()
	}

	if (data.type == "changed") {


		console.log("[Connection count changed]");
		// console.log(data.data);
		// var count = data.data;
		// var ttl = parseInt(count.offline) + parseInt(count.online) + parseInt(count.faulty)
		// $('#totalDev').text(ttl.toString());
		// $('#onlineDev').text(count.online);
		// $('#offlineDev').text(count.offline);
		// $('#faultyDev').text(count.faulty);
		// // $('#echo_stream').text(data.custom.echoCount);
		// // $('#p2p_stream').text(data.custom.p2pCount);
		// // $('#pipe_stream').text(data.custom.pipeCount);


	}

});

stats.io.on("totalmsgcount", function (data) {

	
	if (stats.chart.dataProvider.length > 20) {

		stats.chart.dataProvider.shift();
	}
	var time = Date.now();
    console.log("[-- Active node info---]");
    console.log(data.custom.nodeCount);
    console.log(data.custom.nodeName);
    
	stats.tx_stream_c = parseFloat(data.inmsg);
	stats.rx_stream_c = parseFloat(data.outmsg);
	stats.pipe_stream_c = parseFloat(data.custom.pipeCount);
	stats.p2p_stream_c = parseFloat(data.custom.p2pCount);
	stats.echo_stream_c = parseFloat(data.custom.echoCount);

	/*  For displaying data in upper cells -- data flow rate  */

	$('#tx_stream').text(data.inmsg);
	$('#rx_stream').text(data.outmsg);
	$('#echo_stream').text(data.custom.echoCount);
	$('#p2p_stream').text(data.custom.p2pCount);
	$('#pipe_stream').text(data.custom.pipeCount);
	stats.chart.dataProvider.push({
		time: time,
        inmsg:data.inmsg,
        outmsg:data.outmsg,
		offmsg: data.custom.offCount,
		echomsg: data.custom.echoCount,
		p2pmsg: data.custom.p2pCount,
		pipemsg: data.custom.pipeCount,

	});
	stats.chart.validateData();



})
