var keystone = require("keystone");
var async = require('async');
var _ = require("underscore");
exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);

	var locals = res.locals;

	// Init locals
	locals.section = 'tickets';
	locals.data = {
		devices: [],
		types: [],
	};

	// return res.render('ops');

	// Load all categories
	// view.on('init', function (next) {

	// 	var q = keystone.list('Device').paginate({
	// 			page: req.query.page || 1,
	// 			perPage: 100,
	// 			maxPages: 10,
	// 			filters: {
	// 				user: req.user,
	// 				// author: req.user,
	// 				state: 'active'
	// 			},
	// 		})
	// 		.sort('-updatedAt')
	// 		.populate('user categories');

	// 	// if (locals.data.category) {
	// 	//     q.where('categories').in([locals.data.category]);
	// 	// }

	// 	q.exec(function (err, results) {
	// 		// console.log(results);

	// 		locals.data.devices = results;
	// 		next(err);
	// 	});
	// });



	// view.on('post', function (next) {

	// 	var search = req.query.search || null;
	// 	var data = req.body;
	// 	var node_name = data["search[value]"] || null;
	// 	var order_col = data["order[0][column]"];
	// 	var order_flow = data["order[0][dir]"];
	// 	var col_name = data[`columns[${parseInt(order_col)}][data]`];
	// 	//  p('Col info', col_name, 'Dir ', order_flow);

	// 	var query = {};
	// 	query.query = {};

	// 	query.query.user = req.user;

	// 	if (search) {
	// 		query.query.title = new RegExp(search, 'i')
	// 	}
	// 	query.limit = data.length;
	// 	query.offset = data.start;
	// 	// query.sort = {};
	// 	// query.sort[col_name] = order_flow;


	// 	var q = keystone.list('Device').paginate({
	// 			page: req.query.page || 1,
	// 			perPage: 15,
	// 			maxPages: 10,
	// 			filters: {
	// 				user: req.user,
	// 				// author: req.user,
	// 				state: 'active'
	// 			},
	// 		})
	// 		.sort('-updatedAt')
	// 		.populate('user categories');

	// 	// if (locals.data.category) {
	// 	//     q.where('categories').in([locals.data.category]);
	// 	// }

	// 	q.exec(function (err, results) {

	// 		if (err) {


	// 			res.json(err)


	// 		}

	// 		var main_data = {};
	// 		main_data.draw = parseInt(data.draw);
	// 		main_data.data = results.results || {};
	// 		main_data.recordsTotal = parseInt(results.total || "0");
	// 		main_data.recordsFiltered = parseInt(results.total || "0");

	// 		return res.json(main_data);
	// 	});

	// });

	view.render('tickets');

}
