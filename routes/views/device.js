'use strict';
var keystone = require('keystone');
var async = require('async');
var _ = require('underscore');
var moment = require('moment');
exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);

	var locals = res.locals;
	var Device = keystone.list('Bin');
	var devKey = req.params.name;

	// Init locals
	locals.section = 'bins';
	locals.data = {
		device: {},
		types: {},
	};
	locals.moment = moment;
	view.on('init', function (next) {


		async.series([

			function (n) {

				Device.model.findOne({
					binKey: devKey,
					user: req.user,
				}).populate('user').exec(function (err, dev) {

					if (err) {

						return n(err);

					}


					if (typeof dev === 'undefined' || dev === null) {

						return n({
							success: false,
							err: ' no device found please check the details ',
						});
					}


					// var alert = {
					// 	"faulty": {
					// 		class: "danger",
					// 		lable: "Faulty"
					// 	},
					// 	"online": {
					// 		class: "success",
					// 		lable: "Online"
					// 	},
					// 	"offline": {
					// 		class: "warning",
					// 		lable: "Offline"
					// 	},

					// };

					// return res.json(result);

					locals.data.device = dev;

					next();

				});


			},

		], function (err) {

			if (err) {

				next(err);
			}



		});


	});


	view.render('device');

};
