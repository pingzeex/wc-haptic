var keystone = require("keystone");
var async = require('async');
var _ = require("lodash");
exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);

	var locals = res.locals;

	// Init locals
	locals.section = 'pipes';
	locals.data = {
		pipes: [],
		types: [],
	};

	var Pipe = keystone.list('Pipe');
	var Link = keystone.list('Link');
	view.on('init', function (next) {

		Pipe.model.count({
			user: req.user,
		}, function (err, onCount) {
			if (err) {


				next(err)

			} else {

                		locals.count = {
                			total: onCount ,
                			time: Date.now(),
                		}
                		next();

			}





		});

    });
    
	view.on('init', function (next) {

		Link.model.find({
			user: req.user,
		}).populate({
				path: 'device',
			}).exec(
			function (err, onCount) {
			if (err) {


				next(err)

			} else {
            
                           locals.count.online_device = 0 ;
                           locals.count.offline_device = 0 ;

                     _.forEach(onCount,function(val){

                        console.log(val.device.status);
                        
                        console.log(locals.count);

                                        if (val.device.status == "online") {

                                            locals.count.online_device = locals.count.online_device+1

                                            
                                        }
                                        if (val.device.status == "offline") {

                                              locals.count.offline_device = locals.count.offline_device + 1;
    
                                        }

                         });

                		next();

			}

		});

    });
    
	view.on('init', function (next) {

		Link.model.count({
			user: req.user,
		}, function (err, onCount) {
			if (err) {


				next(err)

			} else {

                		locals.count.link = onCount ;
                		next();

			}





		});

	});



	view.on('post', function (next) {

		var search = req.query.search || null;
		var data = req.body;
		var node_name = data["search[value]"] || null;
		var order_col = data["order[0][column]"];
		var order_flow = data["order[0][dir]"];
		var col_name = data[`columns[${parseInt(order_col)}][data]`];
		 console.log('Col info', col_name, 'Dir ', order_flow);

		var query = {};
		query.query = {};

		query.query.user = req.user;

		if (search) {
			query.query.title = new RegExp(search, 'i')
		}
		query.limit = data.length;
		query.offset = data.start;
		// query.sort = {};
		// query.sort[col_name] = order_flow;


		var q = keystone.list('Pipe').paginate({
				page: req.query.page || 1,
				perPage: 15,
				maxPages: 10,
				filters: {
					user: req.user,
				},
			})
			.sort('-createdDate')

		q.exec(function (err, results) {

			if (err) {


				res.json(err)


			}

			var main_data = {};
			main_data.draw = parseInt(data.draw);
			main_data.data = results.results || {};
			main_data.recordsTotal = parseInt(results.total || "0");
			main_data.recordsFiltered = parseInt(results.total || "0");

			return res.json(main_data);
		});

	});

	view.render('pipes');

}
