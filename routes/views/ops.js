'use strict';
var keystone = require('keystone');
var async = require('async');
var _ = require('underscore');
exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);

	var locals = res.locals;

	// Init locals
	locals.section = 'dash';
	locals.data = {
		devices: [],
		types: [],
	};

	// return res.render('ops');

	/*  getting device object for  */
	var Device = keystone.list('Bin');
	view.on('init', function (next) {

		Device.model.count({
			user: req.user,
			status: 'online',
			state: 'active',
		}, function (err, onCount) {
			if (err) {


				next(err);

			} else {


				Device.model.count({
					user: req.user,
					status: 'offline',
					state: 'active',
				}, function (err, offCount) {


					if (err) {

						next(err);

					} else {


						Device.model.count({
							user: req.user,
							status: 'faulty',
							state: 'active',
						}, function (err, faultCount) {

							if (err) {


								next(err);

							} else {


								locals.count = {
									total: parseInt(onCount) + parseInt(offCount) + parseInt(faultCount),
									time: Date.now(),
									offline: offCount,
									online: onCount,
									faulty: faultCount,
								};
								next();
							}


						});

					}


				});

			}


		});

	});


	// Load all categories
	view.on('init', function (next) {

		var q = keystone.list('Bin').paginate({
			page: req.query.page || 1,
			perPage: 100,
			maxPages: 10,
			filters: {
				user: req.user,
					// author: req.user,
				state: 'active',
			},
		})
			.sort('-updatedAt')
			.populate('user');

		// if (locals.data.category) {
		//     q.where('categories').in([locals.data.category]);
		// }

		q.exec(function (err, results) {
			// console.log(results);

			locals.data.devices = results;
			next(err);
		});
	});


	view.render('ops');

};
