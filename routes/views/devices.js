'use strict';
var keystone = require('keystone');
var async = require('async');
var _ = require('underscore');
exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);

	var locals = res.locals;

	// Init locals
	locals.section = 'devices';
	locals.data = {
		devices: [],
		types: [],
	};

	var Device = keystone.list('Bin');
	view.on('init', function (next) {

		Device.model.count({
			user: req.user,
			status: 'online',
			state: 'active',
		}, function (err, onCount) {
			if (err) {


				next(err);

			} else {


				Device.model.count({
					user: req.user,
					status: 'offline',
					state: 'active',
				}, function (err, offCount) {


					if (err) {

						next(err);

					} else {


						Device.model.count({
							user: req.user,
							status: 'faulty',
							state: 'active',
						}, function (err, faultCount) {

							if (err) {


								next(err);

							} else {


								locals.count = {
									total: parseInt(onCount) + parseInt(offCount) + parseInt(faultCount),
									time: Date.now(),
									offline: offCount,
									online: onCount,
									faulty: faultCount,
								};
								next();
							}


						});

					}


				});

			}


		});

	});


	view.on('post', function (next) {

		var search = req.query.search || null;
		var status = req.query.status || null;
		var data = req.body;
		var node_name = data['search[value]'] || null;
		var order_col = data['order[0][column]'];
		var order_flow = data['order[0][dir]'];
		var col_name = data[`columns[${parseInt(order_col)}][data]`];
		//  p('Col info', col_name, 'Dir ', order_flow);

		var query = {};
		query.query = {};

		query.query.user = req.user;

		if (search) {
			query.query.title = new RegExp(search, 'i');
		}
		query.limit = data.length;
		query.offset = data.start;
		// query.sort = {};
		// query.sort[col_name] = order_flow;

		var filters = {
			user: req.user,
			state: 'active',
		};
		if (status !== null) {
			filters.status = status;
		}
		var q = keystone.list('Bin').paginate({
			page: req.query.page || 1,
			perPage: 15,
			maxPages: 10,
			filters: filters,
		})
			.sort('-updatedAt')
			.populate('user');

		q.exec(function (err, results) {

			if (err) {


				res.json(err);


			}

			var main_data = {};
			main_data.draw = parseInt(data.draw);
			main_data.data = results.results || {};
			main_data.recordsTotal = parseInt(results.total || '0');
			main_data.recordsFiltered = parseInt(results.total || '0');

			return res.json(main_data);
		});

	});

	view.render('devices');

};
