/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	apis: importRoutes('./apis'),
};

// Setup Route Bindings
exports = module.exports = function (app) {
	// Views
	// app.get('/', routes.views.index);
	app.all('/apis/pingzee/signin', routes.apis.main.login);
	app.all('/apis/pingzee/signout', middleware.requireUser, routes.apis.main.logout);
	app.get('/', middleware.requireUser, routes.views.ops);
	app.all('/contact', routes.views.contact);

	/*  User apis */

	/*  Bin profile routes */

	app.all('/bins', middleware.requireUser, routes.views.devices);
	app.all('/bin/:name', middleware.requireUser, routes.views.device);
	app.get('/login', routes.views.login);

	/*  WC Haptic  apis */

	app.get('/apis/list/levels', middleware.requireUser, routes.apis.list.levels);
	app.get('/apis/list/alerts', middleware.requireUser, routes.apis.list.alerts);
	app.get('/apis/bin/info', middleware.requireUser, routes.apis.devices.device);
	app.post('/apis/wc/login', routes.apis.main.login_api);


	// app.get('/apis/bins/add', middleware.requireUser, routes.apis.bookings.createBooking);

	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};
