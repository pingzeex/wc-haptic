/* eslint-disable indent */
var keystone = require('keystone');
var async = require('async');
var moment = require('moment');
var _ = require('underscore');
exports = module.exports = function (req, res) {


	var Device = keystone.list('Bin');
	var Alert = keystone.list('Alert');

	var devParams = req.query;

	var filter = {
		user: req.user,
	};

	if (devParams.key) {

		filter.binKey = devParams.key;
	} else if (devParams.title) {

		filter.title = devParams.title;
	} else {
		filter.binKey = '12345';
	}

	Device.model.findOne(filter).exec(function (err, dev) {

		if (err) {

			return res.json({
				success: false,
				err: err,
			});

		}

		if (dev.notice > 0) {

			var current = dev.notice;

			Alert.model.find({ userKey: dev.user, binKey: dev._id }, function (err, alerts) {

				if (err) {

					return res.json({
						success: false,
						err: err,
					});

				}

				var slice = -Math.abs(current);
				console.log('[ Checking notice]');
				console.log(alerts.slice(slice));
				dev.notice = 0;
				dev.save(function (_err, _devData) {


					console.log('[---Alerts---]');

					console.log(alerts);

					if (typeof dev === 'undefined' || dev === null) {

						return res.json({
							success: false,
							err: ' no device found please check the details ',
						});
					}


					var alert = {
						faulty: {
							class: 'danger',
							lable: 'Faulty',
						},
						online: {
							class: 'success',
							lable: 'Online',
						},
						offline: {
							class: 'warning',
							lable: 'Offline',
						},

					};

					var ops = `<div class="chat-message-right mb-4">
	<div>
		<button type="button" class="btn btn-default btn-round icon-btn mr-1">
			<i class="ion ion-md-cube"></i>
		</button>
		<div class="text-muted small text-nowrap mt-2">${(function () {
							return moment().format('hh:mm:ss a');
						})()}</div>
	</div>
    <div class="flex-shrink-1 bg-lighter rounded py-2 px-3 mr-3">
        <msg>
        <div class="font-weight-semibold mb-1">Bin :  ${dev.title}</div>       
        	    <ul class = "ui-list" >
                <li > Device status  <span class="text-${alert[dev.status].class}">${alert[dev.status].lable}</span></li> 
                <li> Last Updated at ${(function () {
							return moment(dev.updatedAt).format('hh:mm:ss a DD MMM YYYY');
						})()}  </li> 
				</ul>
				</msg>
				
<hr class="border m-3">
<!-- &nbsp; -->
    </div>
    
</div>`;

					var result = {
						success: true,
						data: {
							title: dev.title,
							id: dev._id,
							createdAt: dev.createdDate,
							user: dev.user,
							type: dev.categories,
							alert: true,
							notice: alerts.slice(slice),
						},
						bot: ops,
					};


					return res.json(result);


				});


			});

			/*${(function () {
			
										var str = ' ';
										_.each(alerts[-Math.abs(current)], function (_val, _key) {
											str = str + `<li>${(_val.alertType === 'oLevel' ? 'Organic Level' : 'Inorganic Level')} : ${_val.alertVal}  </li>`;
			
										});
										return str;
									})()} */

		} else {


			if (typeof dev === 'undefined' || dev === null) {

				return res.json({
					success: false,
					err: ' no device found please check the details ',
				});
			}


			var alert = {
				faulty: {
					class: 'danger',
					lable: 'Faulty',
				},
				online: {
					class: 'success',
					lable: 'Online',
				},
				offline: {
					class: 'warning',
					lable: 'Offline',
				},

			};

			var ops = `<div class="chat-message-right mb-4">
<div>
	<button type="button" class="btn btn-default btn-round icon-btn mr-1">
		<i class="ion ion-md-cube"></i>
	</button>
	<div class="text-muted small text-nowrap mt-2">${(function () {
					return moment().format('hh:mm:ss a');
				})()}</div>
</div>
<div class="flex-shrink-1 bg-lighter rounded py-2 px-3 mr-3">
	<msg>
	<div class="font-weight-semibold mb-1">Bin :  ${dev.title}</div>       
			<ul class = "ui-list" >
			<li > Device status  <span class="text-${alert[dev.status].class}">${alert[dev.status].lable}</span></li> 
			<li> Last Updated at ${(function () {
					return moment(dev.updatedAt).format('hh:mm:ss a DD MMM YYYY');
				})()}  </li> 
			</ul>
			</msg>
			
<hr class="border m-3">
<!-- &nbsp; -->
</div>

</div>`;

			var result = {
				success: true,
				data: {
					title: dev.title,
					id: dev._id,
					createdAt: dev.createdDate,
					user: dev.user,
					alert: false,
					type: dev.categories,
				},
				bot: ops,
			};


			return res.json(result);

		}


	});


};
