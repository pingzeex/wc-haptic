var keystone = require('keystone');
var async = require('async');
var moment = require('moment');
var shortid = require('shortid');

exports = module.exports = function (req, res) {

	var Device = keystone.list('Bin');
	var queries = req.query;
	var title = queries.title ? `${queries.title}@${shortid.generate()}` : `pingzee@${shortid.generate()}`;

	keystone.createItems({

		Bin: [

			{
				status: 'offline',
				title: title,
				state: 'active',
				createdDate: Date.now,
				binKey: shortid.generate,
				user: req.user,
			},

		],

	}, {
		verbose: true,
	}, function (err, stats) {
		if (err) { return res.json({
			success: false,
			err: err,
		}); }

		return res.json({
			success: true,
			err: null,
		});

		// console.log('our results', stats);

	});


};
