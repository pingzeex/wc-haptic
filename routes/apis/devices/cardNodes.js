var keystone = require("keystone");
exports = module.exports = function (req, res) {


    var Node = keystone.list('Node');


    var q = keystone.list('Node').paginate({
        page: req.query.page || 1,
        perPage: 10,
        maxPages: 10,
        filters: {
            author: req.user,
            state: 'active'
        },
    }).sort('-createdDate');

    //  q.where('title').equals(query.query.title)
    q.exec(function (err, nodeList) {

        if (err) {


            res.json(err)


        } else {


            //  console.log(nodeList);


            var main_data = {};
        
            main_data.data = nodeList.results || {};

            res.json(main_data);

        }

    });





};