var keystone = require('keystone');
// var async = require('async');
const moment = require('moment');

const today = moment().startOf('day');
exports = module.exports = function (req, res) {

	var Counts = keystone.list('Levels');
	var s = req.query.start;

	var e = req.query.end;
	var key = req.query.key || null;
	console.log(key);

	var start = moment(s, 'MM-DD-YYYY');
	var end = moment(e, 'MM-DD-YYYY').add(1, 'days');
	// console.log(start,end);

	var qry = {
		userKey: req.user._id,
		time: {
			$gte: start.toDate(),
			$lte: end.toDate(),
		},
	};
	if (key) {

		qry.binKey = key;
	}
	// return res.json({'name':'nikhil'});

	Counts.model.find(qry, {
		inoLevel: 1,
		inoLevelBin: 1,
		oLevelBin: 1,
		oLevel: 1,
		time: 1,
		_id: 0,
	}, function (err, onLevel) {
		if (err) {


			return res.json(err);

		} else {

			if (onLevel.length === 0) {

				return res.json({
					data: [{
						onLevel: 0,
						inoLevel: 0,
						time: Date.now(),

					}],
				});

			} else {

				// console.log(onLevel);
				return res.json({
					data: onLevel,
				});

			}

		}


	});


};
