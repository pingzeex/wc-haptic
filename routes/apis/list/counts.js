var keystone = require("keystone");
// var async = require('async');
const moment = require('moment')

const today = moment().startOf('day')
exports = module.exports = function (req, res) {

    var Counts = keystone.list('CountNode');
    var s = req.query.start;

    var e =req.query.end;
    // console.log(s,e);
    
    var start = moment(s, "MM-DD-YYYY");
    var end = moment(e,"MM-DD-YYYY").add(1, "days");
    // console.log(start,end);
    
  

    // return res.json({'name':'nikhil'});

    Counts.model.find({
                userKey: req.user,
                time: {
                    $gte: start.toDate(),
                    $lte: end.toDate()
                }
            }, function (err, onCount) {
        if (err) {


            return res.json(err)

        }

        else{

            if (onCount.length === 0 ) {

               return  res.json({data:[{
                      offline:0,
                      online:0,
                      faulty:0,
                      none:0,
                      time:Date.now()

               }]})
                
            }

            else {


                  return  res.json({data:onCount});

            }
            
        }



    });






};