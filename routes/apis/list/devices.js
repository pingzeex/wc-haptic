var keystone = require("keystone");
var _ = require("underscore");
// var async = require('async');
exports = module.exports = function (req, res) {

	// var Counts = keystone.list('CountNode');

		var q = keystone.list('Device').paginate({
				page: req.query.page || 1,
				perPage: 15,
				maxPages: 10,
				filters: {
					user: req.user,
					// author: req.user,
					state: 'active'
				},
			})
			.sort('-updatedAt')
			.populate('user categories');


		q.exec(function (err, results) {

			var devData = [];

			_.each(results.results, function (val, key) {

				// c = val  ;


				var a = `<a href="javascript:void(0)" data-key=${val.deviceKey}
											class="list-group-item nameList list-group-item-action ${val.status == "online" ? "online":"offline"}">
												<button type="button" class="btn ${val.status == "online" ? "btn-success":"btn-danger"} btn-round icon-btn mr-1">
													<i class="ion ion-md-cube"></i>
												</button>
												<div class="media-body ml-3">
													${val.title}
													<div class="chat-status small">
														<span class="badge badge-dot"></span>&nbsp;
														${val.status == "online" ? "Online":"Offline"}

													</div>


												</div>
												&nbsp;
									
                                            </a>`;

				devData.push({
					device: a
				});


			});
			return res.json({
				data: devData
			});

		});




};
