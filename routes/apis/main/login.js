var keystone = require("keystone"),async = require('async'),
    _ = require('lodash'),
    User = keystone.list('User');

exports = module.exports = function (req, res) {


    // var Node = keystone.list('Node');



    var locals = {
        form: req.body,

        newUser: false
    }

    // Function to handle signin
    var doSignIn = function () {;

        console.log('[api.app.signin.]  - Signing in user...');
        console.log('------------------------------------------------------------');

        var onSuccess = function (user) {
            console.log('[api.app.signup]  - Successfully signed in.');
            console.log('------------------------------------------------------------');
            return res.redirect("/");
        }

        var onFail = function (err) {
            console.log('[api.app.signup]  - Failed signing in.', err);
            console.log('------------------------------------------------------------');
            return res.redirect("/login");
        }

        var form = locals.form ;

        keystone.session.signin(form, req, res, onSuccess, onFail);

    };


        //  Sign Function 

        // doSignIn();


    async.series([

        // Perform basic validation
        function (next) {

            console.log(locals.form);
            
            
            if (!locals.form.email || !locals.form.password ) {
                console.log('[api.app.siginup] - Failed signing up.',locals.form);
                console.log('------------------------------------------------------------');
                return next("err found")
            }

            return next();

        },

        function (next) {

            return doSignIn();
        }

    ], function (err) {
        if (err) {

            console.log('[api.app.signup]  - Issue signing user in.', err);
            console.log('------------------------------------------------------------');
            return res.redirect("/login");
        }
    });



};