// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();

// Require keystone
var keystone = require('keystone');
var cons = require('consolidate');
var nunjucks = require('nunjucks');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
	'name': 'wc-haptic',
	'brand': 'wc-haptic',
	'port': 6001,
	'less': 'public',
	'cookie secret': process.env.COOKIE_SECRET,
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': '.html',
	'custom engine': cons.nunjucks,

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'signout redirect': '/login',
	'signin url': '/login',
});

// Load your project's Models
keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

// Load your project's Routes
keystone.set('routes', require('./routes'));


// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
	enquiries: 'enquiries',
	bins: 'bins',
	users: 'users',
});

// Start Keystone to connect to your database and initialise the web server

// keystone.start();


keystone.start({
	onHttpServerCreated: function () {
		console.log(process.env.DEPLOY_MODE);

		if (process.env.DEPLOY_MODE === 'dev') {
			console.log('running in development mode ');


		}
		if (process.env.DEPLOY_MODE === 'testing') {
			console.log('running in testing mode');
			require('./socket')(keystone.httpServer);

		}
		// if (process.env.DEPLOY_MODE === "production") {

		// 	console.log("running in production mode ");
		// 	var io = require("./socket")(keystone.httpServer)
		// 	require("./redis")();
		// }
	},
});
